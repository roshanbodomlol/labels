import { createMuiTheme } from '@material-ui/core/styles';

const palette = {
  primary: {
    main: '#11100C'
  },
  secondary: {
    main: '#DADADA'
  }
};

const typography = {
  fontFamily: [
    'tex',
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"'
  ].join(',')
};

export default createMuiTheme({
  palette,
  typography
});
