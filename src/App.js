import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router';
import axios from 'axios';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Cookie from 'js-cookie';
import { Helmet } from 'react-helmet';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';

import Header from './Components/Header';
import Home from './Components/Home';
import ListingPage from './Components/ListingPage';
import Product from './Components/Product';
import CartPage from './Components/Cart/CartPage';
import Footer from './Components/Footer';
import Oops from './Components/Oops';
import Brand from './Components/Brand';
import ScrollToTop from './Components/ScrollToTop';
import Logout from './Components/Logout';
import Popup from './Components/Popup';
import Sale from './Components/ListingPage/Sale';
import Wishlist from './Components/Wishlist';
import Privacy from './Components/Privacy';
import FAQ from './Components/FAQ';
import Help from './Components/Help';
import Loyalty from './Components/Loyalty';
import TNC from './Components/TNC';
import LabelCoins from './Components/LabelCoins';
import Accessibilty from './Components/Accessiblity';
import AboutLabels from './Components/AboutLabels';
import AboutUs from './Components/AboutUs';

import { userLogin, hideGlobalSnack } from './Store/actions';
import store from './Store';
import { authenticateUrl } from './globalSettings';

import './Styles/scss/app.scss';

const mapDispatchToProps = dispatch => (
  {
    userLogin: user => dispatch(userLogin(user))
  }
);

const mapStateToProps = state => ({
  globalSnack: state.globalSnack,
  globalSnackMessage: state.globalSnackMessage
});

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount = () => {
    this.refreshUser();
  }

  closeGlobalSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    store.dispatch(hideGlobalSnack());
  }

  refreshUser = () => {
    // const token = localStorage.getItem('lbl_token');
    const token = Cookie.get('lbl_token');
    const { userLogin: userLoginProp } = this.props;
    if (token) {
      const authenticateBody = new FormData();
      authenticateBody.set('token', token.toString());
      axios({
        url: authenticateUrl,
        method: 'POST',
        data: authenticateBody
      })
        .then((response) => {
          if (response.data.type === 'success') {
            userLoginProp(response.data.response);
          } else throw new Error(response.data.message);
        })
        .catch((e) => {
          console.log(e);
          localStorage.removeItem('lbl_token');
        });
    }
  }

  render() {
    const { globalSnack, globalSnackMessage } = this.props;
    return (
      <div id="wrapper" className="theme-summer">
        <Snackbar
          key="global-snack"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={globalSnack}
          autoHideDuration={3000}
          onClose={this.closeGlobalSnack}
        >
          <SnackbarContent
            key="global-snack-content"
            message={<span>{globalSnackMessage}</span>}
            action={(
              <CloseIcon
                style={{ cursor: 'pointer' }}
                key="add-to-wishlist-snack-action"
                onClick={this.closeGlobalSnack}
              />
                  )}
          />
        </Snackbar>
        <Helmet>
          <meta property="og:title" content="LABELS STORE" />
          <meta property="og:description" content="Get the best deals in the best" />
        </Helmet>
        <ScrollToTop />
        <Popup />
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/shop/sale" exact component={Sale} />
          <Route path="/shop/:cat" exact component={ListingPage} />
          <Route path="/product/:slug" exact component={Product} />
          <Route path="/brand/:slug" exact component={Brand} />
          <Route path="/cart" component={CartPage} />
          <Route path="/wishlist" exact component={Wishlist} />
          <Route path="/about-us" exact component={AboutUs} />
          <Route path="/privacy-cookies" exact component={Privacy} />
          <Route path="/about-us-more" exact component={AboutLabels} />
          <Route path="/labels-loyalty-program" exact component={Loyalty} />
          <Route path="/faq" exact component={FAQ} />
          <Route path="/help" exact component={Help} />
          <Route path="/terms-and-conditions" exact component={TNC} />
          <Route path="/accessibility" exact component={Accessibilty} />
          <Route path="/label-coins" exact component={LabelCoins} />
          <Route path="/logout" component={Logout} />
          <Route path="/notfound" exact component={Oops} />
          <Redirect to="/notfound" />
        </Switch>
        <Footer />
      </div>
    );
  }
}

App.propTypes = {
  userLogin: PropTypes.func.isRequired,
  globalSnack: PropTypes.bool.isRequired,
  globalSnackMessage: PropTypes.string.isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
