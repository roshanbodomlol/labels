import React, { Component } from 'react';
import uuid from 'uuid';
import cn from 'classnames';
import Slider from '../ReactAnimeSlider';

import styles from './styles.module.scss';

import slide1 from '../../Static/img/offers-slide-1.jpg';
import slide2 from '../../Static/img/offers-slide-2.jpg';

class OffersSlider extends Component {
  constructor() {
    super();
    this.state = {
      sliders: []
    };
  }

  componentDidMount() {
    this.setState({
      sliders: [
        {
          url: '/',
          img: {
            url: slide1
          },
          highlight: '15% OFF',
          name: 'REEBOK TRAINFLEX'
        },
        {
          url: '/',
          img: {
            url: slide2
          },
          highlight: '35% OFF',
          name: 'REEBOK TRAINFLEX II'
        }
      ]
    });
  }

  render() {
    const { sliders } = this.state;
    const slides = sliders.map(slide => (
      <div className={styles.slide} key={uuid.v4()} style={{ background: `url('${slide.img.url}') 0px center` }}>
        <div className={styles.overlay}>
          <div className={cn('offerSliderTitle', styles.highlight)}>{slide.highlight}</div>
          <div className={styles.name}>{slide.name}</div>
          <a href="/" className="btn btn-light">SHOP</a>
        </div>
      </div>
    ));
    return (
      <div className={styles.slider} id="offers-slider">
        <Slider mobileBreakPoint={769}>
          {slides}
        </Slider>
      </div>
    );
  }
}

export default OffersSlider;
