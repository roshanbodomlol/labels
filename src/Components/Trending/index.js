import React, { Component } from 'react';
import axios from 'axios';

import ThumbnailList from '../Layout/ThumbnailListHorizontal';
import { apiUrl } from '../../globalSettings';

class Trending extends Component {
  constructor() {
    super();
    this.state = {
      trends: []
    };
  }

  componentDidMount() {
    axios
      .get(`${apiUrl}/products/getTrending`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            trends: response.data.response
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  render() {
    const { trends } = this.state;
    return (
      <ThumbnailList title="WHAT'S TRENDING" listItems={trends} />
    );
  }
}

export default Trending;
