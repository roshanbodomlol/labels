import React, { Component } from 'react';
import uuid from 'uuid';

import ReactAnimeSlider from '../ReactAnimeSlider';

import slide3 from '../../Static/img/slider/fox.jpg';
import slide1 from '../../Static/img/slider/flamingo.jpg';
import slide5 from '../../Static/img/slider/pup.jpg';
import slide6 from '../../Static/img/slider/rope.jpg';

class BannerSlider extends Component {
  constructor() {
    super();
    this.state = {
      sliders: []
    };
    this.bannerRatio = (1920 / 700);
  }

  componentDidMount() {
    this.setState({
      sliders: [
        {
          img: {
            url: slide3
          },
          name: 'UNWRAP INSPIRATION',
          description: 'Fitbit motivates you to reach your health and fitness goals by tracking your activity, exercise, sleep, weight and more.',
          url: '/'
        },
        {
          img: {
            url: slide1
          },
          name: 'UNWRAP INSPIRATION',
          description: 'Fitbit motivates you to reach your health and fitness goals by tracking your activity, exercise, sleep, weight and more.',
          url: '/'
        },
        {
          img: {
            url: slide5
          },
          name: 'UNWRAP INSPIRATION',
          description: 'Fitbit motivates you to reach your health and fitness goals by tracking your activity, exercise, sleep, weight and more.',
          url: '/'
        },
        {
          img: {
            url: slide6
          },
          name: 'UNWRAP INSPIRATION',
          description: 'Fitbit motivates you to reach your health and fitness goals by tracking your activity, exercise, sleep, weight and more.'
        }
      ]
    });
  }

  render() {
    const { sliders } = this.state;
    const allSlides = sliders.map(slide => (
      <div key={`banner-slider-${uuid.v4()}`} className="banner" style={{ background: `url('${slide.img.url}') 0px center` }}>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <div className="details">
                {
                  slide.name
                    ? (
                      <h1>{slide.name}</h1>
                    )
                    : ''
                }
                {
                  slide.description
                    ? (
                      <p>{slide.description}</p>
                    )
                    : ''
                }
                {
                  slide.url
                    ? (
                      <a href={slide.url} className="btn __dark">Shop</a>
                    )
                    : ''
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    ));
    return (
      <div id="banner-slider">
        <ReactAnimeSlider
          duration={750}
          height={600}
          easing="easeOutQuint"
          autoplay
          interval={3000}
          arrows={false}
          pauseOnHover
          ratio={this.bannerRatio}
        >
          {allSlides}
        </ReactAnimeSlider>
      </div>
    );
  }
}

export default BannerSlider;
