import React from 'react';

import styles from './styles.module.scss';

import catMen from '../../Static/img/cat-men.jpg';
import catWomen from '../../Static/img/cat-women.jpg';
import catKids from '../../Static/img/cat-kids.jpg';

const CatLinks = () => (
  <div id="cat-links" className={styles.cats}>
    <div className="row">
      <div className="col-sm">
        <div className={styles.cat}>
          <a href="/">
            <img src={catMen} alt="" />
            <div className={styles.overlay}>
              <span>MEN</span>
            </div>
          </a>
        </div>
      </div>
      <div className="col-sm">
        <div className={styles.cat}>
          <a href="/">
            <img src={catWomen} alt="" />
            <div className={styles.overlay}>
              <span>WOMEN</span>
            </div>
          </a>
        </div>
      </div>
      <div className="col-sm">
        <div className={styles.cat}>
          <a href="/">
            <img src={catKids} alt="" />
            <div className={styles.overlay}>
              <span>KIDS</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default CatLinks;
