import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import uuid from 'uuid';
import { instagramIdToUrlSegment } from 'instagram-id-to-url-segment';

import styles from './styles.module.scss';

class InstagramFeed extends Component {
  constructor() {
    super();
    this.state = {
      posts: []
    };
  }

  componentDidMount() {
    const { thumbnailWidth, limit, hashtag } = this.props;
    axios.get(`https://www.instagram.com/explore/tags/${hashtag}/?__a=1`)
      .then((response) => {
        // console.log(response);
        const nodes = response.data.graphql.hashtag.edge_hashtag_to_top_posts.edges.slice(0, limit);
        const posts = [];
        nodes.forEach((node) => {
          const thumbnails = node.node.thumbnail_resources;
          for (let i = 0; i < thumbnails.length; i++) {
            if (thumbnails[i].config_width > thumbnailWidth) {
              if (i > 0) {
                posts.push({
                  img: thumbnails[i - 1],
                  urlID: instagramIdToUrlSegment(node.node.id.toString())
                });
              } else {
                posts.push({
                  img: thumbnails[0],
                  urlID: instagramIdToUrlSegment(node.node.id.toString())
                });
              }
              break;
            }
          }
        });
        this.setState({
          posts
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { hashtag } = this.props;
    const { posts } = this.state;
    const feed = posts.map(post => (
      <div key={`insta-feed-${uuid.v4()}`} className="col insta-col">
        <div className={styles.feed}>
          <a href={`https://instagram.com/p/${post.urlID}`} target="_blank" rel="noopener noreferrer">
            <img src={post.img.src} alt="" />
          </a>
        </div>
      </div>
    ));
    return (
      <div className="row">
        <div className="col">
          <div className={styles.hashtag}>
            <span>
              #
              {hashtag}
            </span>
          </div>
        </div>
        {feed}
      </div>
    );
  }
}

InstagramFeed.propTypes = {
  thumbnailWidth: PropTypes.number,
  limit: PropTypes.number,
  hashtag: PropTypes.string.isRequired
};

InstagramFeed.defaultProps = {
  thumbnailWidth: 320,
  limit: 5
};

export default InstagramFeed;
