import React from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';

const Input = ({
  errors, type, placeholder, onChange, onClick, value, name, readOnly, required, help, other
}) => {
  const fieldError = _.property(name)(errors);
  const inputError = fieldError && (
    <div className="with-error">
      { fieldError }
    </div>
  );
  return (
    <div className="input-wrap">
      {inputError}
      {
        help
          ? <small>{help}</small>
          : ''
      }
      <input
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        readOnly={readOnly}
        required={required}
        onClick={onClick}
      />
      {
        required
          ? (
            <div className="input-required">*</div>
          )
          : ''
      }
      {other}
    </div>
  );
};

Input.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(['text', 'email', 'tel', 'password']).isRequired,
  errors: PropTypes.object,
  name: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  help: PropTypes.string,
  other: PropTypes.node
};

Input.defaultProps = {
  value: '',
  placeholder: '',
  onChange: () => {},
  onClick: () => {},
  errors: null,
  readOnly: false,
  required: false,
  help: null,
  other: null
};

export default Input;
