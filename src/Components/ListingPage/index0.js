import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import qs from 'query-string';
import axios from 'axios';
import store from '../../Store';
import { addToCart } from '../../Store/actions';

import Wrapper, { PlaceholderDiv, PlaceholderSpan, PlaceholderFlex } from '../LoadingPlaceholder';
import { apiUrl } from '../../globalSettings';
import ColumnList from '../Layout/ColumnList';
import Item from './Item';
import Selector from '../Layout/Selector';

import Styles from './styles.module.scss';

import img10 from '../../Static/img/products/beach.jpg';
import img11 from '../../Static/img/products/canyon.jpg';
import img12 from '../../Static/img/products/chameleon.jpg';
import img13 from '../../Static/img/products/deer.jpg';
import img14 from '../../Static/img/products/skii.jpg';
import img15 from '../../Static/img/products/snoww.jpg';
import img16 from '../../Static/img/products/volcano water.jpg';

class ListingPage extends Component {
  constructor() {
    super();
    this.state = {
      subCats: [],
      activeSubCats: [],
      activeChildCats: [],
      sizes: [],
      activeSizes: [],
      brands: [],
      activeBrands: [],
      catsLoaded: false,
      sizesLoaded: false,
      brandsLoaded: false,
      products: [],
      productsLoaded: false,
      sortOption: {
        name: 'Latest',
        value: 'latest'
      },
      sizeOption: {
        name: 'US',
        value: 'us'
      }
    };
    this.sortOptions = [
      {
        name: 'Latest',
        value: 'latest'
      },
      {
        name: 'Popular',
        value: 'popular'
      },
      {
        name: 'Price High To Low',
        value: 'price-desc'
      },
      {
        name: 'Price Low To High',
        value: 'price-asc'
      }
    ];
    this.sizeOptions = [
      {
        name: 'US',
        value: 'us'
      },
      {
        name: 'UK',
        value: 'uk'
      }
    ];
  }

  componentDidMount() {
    this.getCats();
    this.getBrands();
    this.getSizes();
    this.getProducts();
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (prevProps.location.search !== location.search) {
      this.getProducts();
    }
  }

  getSizes = () => {
    this.setState({
      sizes: [
        {
          name: 'XS',
          slug: 'xs'
        },
        {
          name: 'S',
          slug: 's'
        },
        {
          name: 'M',
          slug: 'm'
        },
        {
          name: 'L',
          slug: 'l'
        },
        {
          name: 'XL',
          slug: 'xl'
        }
      ],
      sizesLoaded: true
    });
  }

  getBrands = () => {
    this.setState({
      brands: [
        {
          name: 'Adidas',
          slug: 'adidas',
          id: '1'
        },
        {
          name: 'Reebok',
          slug: 'reebok',
          id: '1'
        },
        {
          name: 'Levi\'s',
          slug: 'levis',
          id: '1'
        },
        {
          name: 'Under Armour',
          slug: 'under-armor',
          id: '1'
        },
        {
          name: 'Fitbit',
          slug: 'fitbit',
          id: '1'
        },
        {
          name: 'Cube',
          slug: 'cube',
          id: '1'
        }
      ],
      brandsLoaded: true
    });
  }

  getCats = () => {
    // const { match } = this.props;
    // const mainCat = match.params.cat;
    axios
      .get(`${apiUrl}/product_category`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            subCats: response.data.response,
            productsLoaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
    this.setState({
      catsLoaded: true
    });
  }

  getProducts = () => {
    const { location } = this.props;
    const { pc } = qs.parse(location.search);
    axios
      .get(`${apiUrl}/categories/${pc}`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            products: response.data.response,
            productsLoaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  loadMore = () => {
    const { products } = this.state;
    const newProducts = [
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img10,
          width: 295,
          height: 371
        }
      },
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img11,
          width: 295,
          height: 371
        }
      },
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img12,
          width: 295,
          height: 371
        }
      },
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img13,
          width: 295,
          height: 371
        }
      },
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img14,
          width: 295,
          height: 371
        }
      },
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img15,
          width: 295,
          height: 371
        }
      },
      {
        name: 'Men\'s Training Freelift Tee',
        slug: 'prd-1',
        id: '1',
        img: {
          url: img16,
          width: 295,
          height: 371
        }
      }
    ];
    this.setState({
      products: [...products, ...newProducts]
    });
  }

  handleActiveCat = (e, catID, parentID) => {
    e.stopPropagation();
    const { activeSubCats, subCats, activeChildCats } = this.state;
    if (e.currentTarget.classList.contains('subCatLi')) {
      if (_.indexOf(activeSubCats, catID) < 0) {
        this.setState({
          activeSubCats: [...activeSubCats, catID]
        });
      } else {
        // deactivate child cats
        const myChildCats = _.findWhere(subCats, { id: catID });
        const myChildCatSlugs = _.pluck(myChildCats.childCats, 'id');
        this.setState({
          activeSubCats: _.without(activeSubCats, catID),
          activeChildCats: _.without(activeChildCats, ...myChildCatSlugs)
        });
      }
    } else if (e.currentTarget.classList.contains('childCatLi')) {
      // check for parent in active list
      if (_.indexOf(activeSubCats, parentID) < 0) {
        this.setState({
          activeSubCats: [...activeSubCats, parentID]
        });
      }
      if (_.indexOf(activeChildCats, catID) < 0) {
        this.setState({
          activeChildCats: [...activeChildCats, catID]
        });
      } else {
        this.setState({
          activeChildCats: _.without(activeChildCats, catID)
        });
      }
    }
  }

  handleActiveSize = (sizeSlug) => {
    const { activeSizes } = this.state;
    if (_.indexOf(activeSizes, sizeSlug) < 0) {
      this.setState({
        activeSizes: [...activeSizes, sizeSlug]
      });
    } else {
      this.setState({
        activeSizes: _.without(activeSizes, sizeSlug)
      });
    }
  }

  handleActiveBrand = (brandSlug) => {
    const { activeBrands } = this.state;
    if (_.indexOf(activeBrands, brandSlug) < 0) {
      this.setState({
        activeBrands: [...activeBrands, brandSlug]
      });
    } else {
      this.setState({
        activeBrands: _.without(activeBrands, brandSlug)
      });
    }
  }

  handleSortUpdate = (option) => {
    this.setState({
      sortOption: option
    });
  }

  handleSizeUpdate = (option) => {
    this.setState({
      sizeOption: option
    });
  }

  addToCart = (item) => {
    store.dispatch(addToCart(item));
  }

  render() {
    const { match } = this.props;
    const {
      subCats,
      activeChildCats,
      activeSubCats,
      sizes,
      activeSizes,
      brands,
      activeBrands,
      products,
      catsLoaded,
      sizesLoaded,
      brandsLoaded,
      productsLoaded,
      sortOption,
      sizeOption
    } = this.state;
    const catName = match.params.cat === 'kids'
      ? `${match.params.cat}'`
      : `${match.params.cat}'s`;
    const subCatsList = subCats.map((subCat, index) => {
      const childCats = subCat.childCats && subCat.childCats.map((childCat, childIndex) => (
        <li
          key={`child-cat-${childIndex}`}
          className={_.indexOf(activeChildCats, childCat.id) > -1 ? 'active childCatLi' : 'childCatLi'}
          role="button"
          tabIndex="-1"
          onClick={(e) => { this.handleActiveCat(e, childCat.id, subCat.id); }}
        >
          <span>{childCat.name}</span>
        </li>
      ));
      return (
        <li
          key={`sub-cat-${index}`}
          role="button"
          tabIndex="-1"
          className={_.indexOf(activeSubCats, subCat.id) > -1 ? 'active subCatLi' : 'subCatLi'}
          onClick={(e) => { this.handleActiveCat(e, subCat.id); }}
        >
          <span>{ subCat.name }</span>
          { subCat.childCats ? <ul>{childCats}</ul> : ''}
        </li>
      );
    });
    const sizeButtons = sizes.map((btn, index) => (
      <button
        key={`size-btn-${index}`}
        onClick={() => { this.handleActiveSize(btn.slug); }}
        className={_.indexOf(activeSizes, btn.slug) < 0 ? 'btn __small' : 'active btn __small'}
      >
        {btn.name}
      </button>
    ));
    const brandSelects = brands.map((brand, index) => (
      <li
        key={`brand-select-${index}`}
        role="button"
        className={_.indexOf(activeBrands, brand.slug) < 0 ? '' : 'active'}
        onClick={() => { this.handleActiveBrand(brand.slug); }}
      >
        <span>{brand.name}</span>
      </li>
    ));
    const productList = products.map(product => (
      <Item key={`listing-item-${product.slug}`} item={product} />
    ));
    return (
      <div id="main">
        <div className="container cust-container">
          <div className="row spacey">
            <div className="col-sm-3" id="sideBarCol">
              <div className={Styles.sidebar}>
                {
                  catsLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          { catName }
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <ul>
                            {subCatsList}
                          </ul>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testtest</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
                {
                  sizesLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          <span>Sizes</span>
                          <div className={Styles.smallSelect}>
                            <Selector
                              options={this.sizeOptions}
                              updateSelect={(option) => { this.handleSizeUpdate(option); }}
                              selected={sizeOption}
                            />
                          </div>
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <div className={Styles.sizeButtons}>
                            {sizeButtons}
                          </div>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testtest</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <div className={Styles.sizeButtons}>
                              <PlaceholderFlex>Test</PlaceholderFlex>
                              <PlaceholderFlex>Test</PlaceholderFlex>
                              <PlaceholderFlex>Test</PlaceholderFlex>
                              <PlaceholderFlex>Test</PlaceholderFlex>
                              <PlaceholderFlex>Test</PlaceholderFlex>
                            </div>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
                {
                  brandsLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          Brands
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <ul>
                            {brandSelects}
                          </ul>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testestt</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
              </div>
            </div>
            <div className="col-sm-9">
              <div className={Styles.listingTop}>
                <Selector
                  options={this.sortOptions}
                  updateSelect={(option) => { this.handleSortUpdate(option); }}
                  selected={sortOption}
                  title="SORT BY: "
                />
              </div>
              <div className={Styles.listing}>
                {
                    productsLoaded
                      ? (
                        <ColumnList cols={4}>
                          {productList}
                        </ColumnList>
                      )
                      : (
                        <div className="row">
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                        </div>
                      )
                  }
              </div>
              <div className={Styles.moreWrap}>
                {
                  productsLoaded
                    ? <button className="btn __dark" onClick={this.loadMore}>LOAD MORE</button>
                    : ''
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListingPage.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired // eslint-disable-line
};

export default ListingPage;
