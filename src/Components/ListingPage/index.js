import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import qs from 'query-string';
import axios from 'axios';
import store from '../../Store';
import { addToCart } from '../../Store/actions';

import Wrapper, { PlaceholderDiv, PlaceholderSpan } from '../LoadingPlaceholder';
import { apiUrl, getBrands } from '../../globalSettings';
import ColumnList from '../Layout/ColumnList';
import Item from './Item';
import Selector from '../Layout/Selector';

import Styles from './styles.module.scss';

class ListingPage extends Component {
  constructor() {
    super();
    this.state = {
      subCats: [],
      activeSubCats: [],
      activeChildCats: [],
      brands: [],
      activeBrands: [],
      catsLoaded: false,
      brandsLoaded: false,
      products: [],
      productsLoaded: false,
      sortOption: {
        name: 'Latest',
        value: 'latest'
      }
    };
    this.sortOptions = [
      {
        name: 'Latest',
        value: 'latest'
      },
      {
        name: 'Price High To Low',
        value: 'price-desc'
      },
      {
        name: 'Price Low To High',
        value: 'price-asc'
      }
    ];
  }

  componentDidMount() {
    const { location } = this.props;
    const { pc } = qs.parse(location.search);
    this.setState({
      activeSubCats: [pc]
    });
    this.getCats();
    this.getBrands();
    this.getProducts();
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (!_.isEqual(location, prevProps.location)) {
      this.resetCategoryFilters();
      this.getProducts();
    }
  }

  resetCategoryFilters = () => {
    const { location } = this.props;
    const { pc } = qs.parse(location.search);
    this.setState({
      activeSubCats: [pc]
    });
  }

  getBrands = () => {
    axios({
      url: getBrands,
      method: 'GET'
    })
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({ brands: response.data.response, brandsLoaded: true });
        } else throw new Error('Cannot retrieve brands');
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getCats = () => {
    const { match } = this.props;
    const mainCat = match.params.cat;
    axios
      .get(`${apiUrl}/product_category`)
      .then((response) => {
        if (response.data.type === 'success') {
          // get maincat children
          const subCats = _.findWhere(
            response.data.response, { category_slug: mainCat }
          );
          this.setState({
            subCats: subCats.children,
            productsLoaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
    this.setState({
      catsLoaded: true
    });
  }

  getProducts = () => {
    this.setState({ products: [], productsLoaded: false });
    const { location, match } = this.props;
    const {
      activeSubCats, activeChildCats, activeBrands, sortOption
    } = this.state;
    const { pc } = qs.parse(location.search);
    axios({
      url: `${apiUrl}/categories`,
      params: {
        category: match.params.cat,
        children: [pc, ...activeSubCats, ...activeChildCats],
        brands: [...activeBrands],
        sorting: sortOption.value
      }
    })
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            products: response.data.response,
            productsLoaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
        this.setState({ productsLoaded: true });
      });
  }

  handleActiveCat = (e, catID, parentID) => {
    e.stopPropagation();
    const { activeSubCats, subCats, activeChildCats } = this.state;
    if (e.currentTarget.classList.contains('subCatLi')) {
      if (_.indexOf(activeSubCats, catID) < 0) {
        this.setState({
          activeSubCats: [...activeSubCats, catID]
        });
        this.getProducts();
      } else {
        // deactivate child cats
        const myChildCats = _.findWhere(subCats, { category_slug: catID });
        const myChildCatSlugs = _.pluck(myChildCats.childCats, 'category_slug');
        this.setState({
          activeSubCats: _.without(activeSubCats, catID),
          activeChildCats: _.without(activeChildCats, ...myChildCatSlugs)
        });
        this.getProducts();
      }
    } else if (e.currentTarget.classList.contains('childCatLi')) {
      // check for parent in active list
      if (_.indexOf(activeSubCats, parentID) < 0) {
        this.setState({
          activeSubCats: [...activeSubCats, parentID]
        });
        this.getProducts();
      }
      if (_.indexOf(activeChildCats, catID) < 0) {
        this.setState({
          activeChildCats: [...activeChildCats, catID]
        });
        this.getProducts();
      } else {
        this.setState({
          activeChildCats: _.without(activeChildCats, catID)
        });
        this.getProducts();
      }
    }
  }

  handleActiveBrand = (brandSlug) => {
    const { activeBrands } = this.state;
    if (_.indexOf(activeBrands, brandSlug) < 0) {
      this.setState({
        activeBrands: [...activeBrands, brandSlug]
      });
      this.getProducts();
    } else {
      this.setState({
        activeBrands: _.without(activeBrands, brandSlug)
      });
      this.getProducts();
    }
  }

  handleSortUpdate = (option) => {
    this.setState({
      sortOption: option
    });
    this.getProducts();
  }

  addToCart = (item) => {
    store.dispatch(addToCart(item));
  }

  render() {
    const { match } = this.props;
    const {
      subCats,
      activeChildCats,
      activeSubCats,
      brands,
      activeBrands,
      products,
      catsLoaded,
      brandsLoaded,
      productsLoaded,
      sortOption
    } = this.state;
    const catName = match.params.cat === 'kids'
      ? `${match.params.cat}'`
      : `${match.params.cat}'s`;
    const subCatsList = subCats.map((subCat, index) => {
      const childCats = subCat.children && subCat.children.map((childCat, childIndex) => (
        <li
          key={`child-cat-${childIndex}`}
          className={_.indexOf(activeChildCats, childCat.category_slug) > -1 ? 'active childCatLi' : 'childCatLi'}
          role="button"
          tabIndex="-1"
          onClick={(e) => {
            this.handleActiveCat(e, childCat.category_slug, subCat.category_slug);
          }}
        >
          <span>{childCat.category_name}</span>
        </li>
      ));
      return (
        <li
          key={`sub-cat-${index}`}
          role="button"
          tabIndex="-1"
          className={_.indexOf(activeSubCats, subCat.category_slug) > -1 ? 'active subCatLi' : 'subCatLi'}
          onClick={(e) => { this.handleActiveCat(e, subCat.category_slug); }}
        >
          <span>{ subCat.category_name }</span>
          { subCat.children ? <ul>{childCats}</ul> : ''}
        </li>
      );
    });
    const brandSelects = brands.map((brand, index) => (
      <li
        key={`brand-select-${index}`}
        role="button"
        className={_.indexOf(activeBrands, brand.brand_slug) < 0 ? '' : 'active'}
        onClick={() => { this.handleActiveBrand(brand.brand_slug); }}
      >
        <span>{brand.brand_name}</span>
      </li>
    ));
    const productList = products.map(product => (
      <Item key={`listing-item-${product.slug}`} item={product} />
    ));
    return (
      <div id="main">
        <div className="container cust-container">
          <div className="row spacey">
            <div className="col-md-3" id="sideBarCol">
              <div className={Styles.sidebar}>
                {
                  catsLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          { catName }
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <ul>
                            {subCatsList}
                          </ul>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testtest</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
                {
                  brandsLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          Brands
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <ul>
                            {brandSelects}
                          </ul>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testestt</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
              </div>
            </div>
            <div className="col-md-9">
              <div className={Styles.listingTop}>
                <Selector
                  options={this.sortOptions}
                  updateSelect={(option) => { this.handleSortUpdate(option); }}
                  selected={sortOption}
                  title="SORT BY: "
                />
              </div>
              <div className={Styles.listing}>
                {
                    productsLoaded
                      ? (
                        <>
                          {
                            productList.length > 0
                              ? (
                                <ColumnList cols={4}>
                                  {productList}
                                </ColumnList>
                              )
                              : (
                                <div className="no-products">
                                  <span>No products found</span>
                                </div>
                              )
                          }
                        </>
                      )
                      : (
                        <div className="row">
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-md-3 col-sm-6">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                        </div>
                      )
                  }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListingPage.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired // eslint-disable-line
};

export default ListingPage;
