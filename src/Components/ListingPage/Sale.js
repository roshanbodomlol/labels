import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import qs from 'query-string';
import axios from 'axios';
import store from '../../Store';
import { addToCart } from '../../Store/actions';

import Wrapper, { PlaceholderDiv, PlaceholderSpan } from '../LoadingPlaceholder';
import { apiUrl, getBrands } from '../../globalSettings';
import ColumnList from '../Layout/ColumnList';
import Item from './Item';
import Selector from '../Layout/Selector';
import ReactAnimeSlider from '../ReactAnimeSlider';

import Styles from './styles.module.scss';

import slide1 from '../../Static/img/slider/sales2.jpg';

class Sale extends Component {
  constructor() {
    super();
    this.state = {
      subCats: [],
      activeSubCats: [],
      activeChildCats: [],
      activeSizes: [],
      brands: [],
      activeBrands: [],
      catsLoaded: false,
      brandsLoaded: false,
      products: [],
      productsLoaded: false,
      sortOption: {
        name: 'Latest',
        value: 'latest'
      },
      slides: []
    };
    this.sortOptions = [
      {
        name: 'Latest',
        value: 'latest'
      },
      {
        name: 'Price High To Low',
        value: 'price-desc'
      },
      {
        name: 'Price Low To High',
        value: 'price-asc'
      }
    ];
    this.bannerRatio = 4.21978021978022;
  }

  componentDidMount() {
    this.getCats();
    this.getBrands();
    this.getProducts();
    this.setState({
      slides: [
        {
          img: {
            url: slide1
          }
        }
      ]
    });
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (prevProps.location.search !== location.search) {
      this.getProducts();
    }
  }

  getBrands = () => {
    axios({
      url: getBrands,
      method: 'GET'
    })
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({ brands: response.data.response, brandsLoaded: true });
        } else throw new Error('Cannot retrieve brands');
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getCats = () => {
    // const { match } = this.props;
    // const mainCat = match.params.cat;
    axios
      .get(`${apiUrl}/product_category`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            subCats: response.data.response,
            productsLoaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
    this.setState({
      catsLoaded: true
    });
  }

  getProducts = () => {
    const { location } = this.props;
    const { pc } = qs.parse(location.search);
    axios
      .get(`${apiUrl}/categories/${pc}`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            products: response.data.response,
            productsLoaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  handleActiveCat = (e, catID, parentID) => {
    e.stopPropagation();
    const { activeSubCats, subCats, activeChildCats } = this.state;
    if (e.currentTarget.classList.contains('subCatLi')) {
      if (_.indexOf(activeSubCats, catID) < 0) {
        this.setState({
          activeSubCats: [...activeSubCats, catID]
        });
      } else {
        // deactivate child cats
        const myChildCats = _.findWhere(subCats, { id: catID });
        const myChildCatSlugs = _.pluck(myChildCats.childCats, 'id');
        this.setState({
          activeSubCats: _.without(activeSubCats, catID),
          activeChildCats: _.without(activeChildCats, ...myChildCatSlugs)
        });
      }
    } else if (e.currentTarget.classList.contains('childCatLi')) {
      // check for parent in active list
      if (_.indexOf(activeSubCats, parentID) < 0) {
        this.setState({
          activeSubCats: [...activeSubCats, parentID]
        });
      }
      if (_.indexOf(activeChildCats, catID) < 0) {
        this.setState({
          activeChildCats: [...activeChildCats, catID]
        });
      } else {
        this.setState({
          activeChildCats: _.without(activeChildCats, catID)
        });
      }
    }
  }

  handleActiveSize = (sizeSlug) => {
    const { activeSizes } = this.state;
    if (_.indexOf(activeSizes, sizeSlug) < 0) {
      this.setState({
        activeSizes: [...activeSizes, sizeSlug]
      });
    } else {
      this.setState({
        activeSizes: _.without(activeSizes, sizeSlug)
      });
    }
  }

  handleActiveBrand = (brandSlug) => {
    const { activeBrands } = this.state;
    if (_.indexOf(activeBrands, brandSlug) < 0) {
      this.setState({
        activeBrands: [...activeBrands, brandSlug]
      });
    } else {
      this.setState({
        activeBrands: _.without(activeBrands, brandSlug)
      });
    }
  }

  handleSortUpdate = (option) => {
    this.setState({
      sortOption: option
    });
  }

  addToCart = (item) => {
    store.dispatch(addToCart(item));
  }

  render() {
    const {
      subCats,
      activeChildCats,
      activeSubCats,
      brands,
      activeBrands,
      products,
      catsLoaded,
      brandsLoaded,
      productsLoaded,
      sortOption,
      slides
    } = this.state;
    const catName = 'SALE';
    const subCatsList = subCats.map((subCat, index) => {
      const childCats = subCat.childCats && subCat.childCats.map((childCat, childIndex) => (
        <li
          key={`child-cat-${childIndex}`}
          className={_.indexOf(activeChildCats, childCat.id) > -1 ? 'active childCatLi' : 'childCatLi'}
          role="button"
          tabIndex="-1"
          onClick={(e) => { this.handleActiveCat(e, childCat.id, subCat.id); }}
        >
          <span>{childCat.name}</span>
        </li>
      ));
      return (
        <li
          key={`sub-cat-${index}`}
          role="button"
          tabIndex="-1"
          className={_.indexOf(activeSubCats, subCat.id) > -1 ? 'active subCatLi' : 'subCatLi'}
          onClick={(e) => { this.handleActiveCat(e, subCat.id); }}
        >
          <span>{ subCat.name }</span>
          { subCat.childCats ? <ul>{childCats}</ul> : ''}
        </li>
      );
    });
    const brandSelects = brands.map((brand, index) => (
      <li
        key={`brand-select-${index}`}
        role="button"
        className={_.indexOf(activeBrands, brand.brand_slug) < 0 ? '' : 'active'}
        onClick={() => { this.handleActiveBrand(brand.brand_slug); }}
      >
        <span>{brand.brand_name}</span>
      </li>
    ));
    const productList = products.map(product => (
      <Item key={`listing-item-${product.slug}`} item={product} />
    ));
    const bannerSlides = slides.map((slide, index) => (
      <div key={`sales-slider-${index}`} className="banner" style={{ background: `url('${slide.img.url}') 0px center` }} />
    ));
    return (
      <div id="main" className={Styles.salesMain}>
        <div className={Styles.saleBanner}>
          <ReactAnimeSlider
            duration={750}
            easing="easeOutQuint"
            autoplay
            interval={3000}
            arrows={false}
            pauseOnHover
            ratio={this.bannerRatio}
          >
            {bannerSlides}
          </ReactAnimeSlider>
        </div>
        <div className="container cust-container">
          <div className="row spacey">
            <div className="col-sm-3" id="sideBarCol">
              <div className={Styles.sidebar}>
                {
                  catsLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          { catName }
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <ul>
                            {subCatsList}
                          </ul>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testtest</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
                {
                  brandsLoaded
                    ? (
                      <div className={Styles.sideBlock}>
                        <div className={Styles.sideBlockTitle}>
                          Brands
                        </div>
                        <div className={Styles.sideBlockMain}>
                          <ul>
                            {brandSelects}
                          </ul>
                        </div>
                      </div>
                    )
                    : (
                      <Wrapper>
                        <div className={Styles.sideBlock}>
                          <div className={Styles.sideBlockTitle}>
                            <PlaceholderSpan>Testestt</PlaceholderSpan>
                          </div>
                          <div className={Styles.sideBlockMain}>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                            <PlaceholderDiv>Test Line</PlaceholderDiv>
                          </div>
                        </div>
                      </Wrapper>
                    )
                }
              </div>
            </div>
            <div className="col-sm-9">
              <div className={Styles.listingTop}>
                <Selector
                  options={this.sortOptions}
                  updateSelect={(option) => { this.handleSortUpdate(option); }}
                  selected={sortOption}
                  title="SORT BY: "
                />
              </div>
              <div className={Styles.listing}>
                {
                    productsLoaded
                      ? (
                        <ColumnList cols={4}>
                          {productList}
                        </ColumnList>
                      )
                      : (
                        <div className="row">
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                          <div className="col-sm-3">
                            <Wrapper>
                              <div className={Styles.product}>
                                <PlaceholderDiv height={350} />
                                <div>
                                  <PlaceholderSpan>This is the placeholder text</PlaceholderSpan>
                                </div>
                                <PlaceholderSpan>This text</PlaceholderSpan>
                              </div>
                            </Wrapper>
                          </div>
                        </div>
                      )
                  }
              </div>
              <div className={Styles.moreWrap}>
                {/* {
                  productsLoaded
                    ? <button className="btn __dark" onClick={this.loadMore}>LOAD MORE</button>
                    : ''
                } */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Sale.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired // eslint-disable-line
};

export default Sale;
