import React, { Component } from 'react';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';
import HeartIcon from '@material-ui/icons/FavoriteBorder';
import RedHeartIcon from '@material-ui/icons/Favorite';
import CartIcon from '@material-ui/icons/ShoppingCartOutlined';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import Cookie from 'js-cookie';
import _ from 'underscore';

import store from '../../Store';
import { addToWishList, removeWishlistItem, addToCart } from '../../Store/actions';
import Styles from './styles.module.scss';

const iconStyles = {
  cursor: 'pointer',
  marginBottom: 15
};

const redIconStyles = {
  cursor: 'pointer',
  marginBottom: 15,
  color: '#ac0826'
};

const tooltipClasses = {
  tooltip: {
    backgroundColor: '#11100C'
  }
};

const addToCartCloseIconStyles = {
  cursor: 'pointer',
  color: 'rgb(136, 136, 136)',
  position: 'absolute',
  right: 6,
  top: 15
};

class Item extends Component {
  state = {
    showSizes: false
  }

  handleWishlistClick = () => {
    const { item } = this.props;
    const wishlistItem = {
      slug: item.slug
    };
    store.dispatch(addToWishList(wishlistItem));
  };

  handleRemoveWishlist = () => {
    const { item } = this.props;
    const wishlistCookie = Cookie.get('labelsWishlist');
    const wishlistArray = wishlistCookie && JSON.parse(wishlistCookie);
    const itemInWishlist = _.find(wishlistArray, wishlistItem => wishlistItem.slug === item.slug);
    store.dispatch(removeWishlistItem(itemInWishlist.wishlistItemID));
  }

  showSizes = () => {
    this.setState({
      showSizes: true
    });
  }

  closeSizes = () => {
    this.setState({
      showSizes: false
    });
  }

  quickAddToCart = (size) => {
    const { item } = this.props;
    store.dispatch(addToCart({
      slug: item.slug,
      quantity: 1,
      price: item.price,
      size
    }));
    this.closeSizes();
  }

  render() {
    const { item, classes } = this.props;
    const { showSizes } = this.state;
    const thumbnailRatio = item.img.width / item.img.height;
    const paddingBottom = (1 / thumbnailRatio) * 100;
    const wishlistCookie = Cookie.get('labelsWishlist');
    const wishlistArray = wishlistCookie && JSON.parse(wishlistCookie);
    const isInWishlist = wishlistArray
      && _.find(wishlistArray, wishlistItem => wishlistItem.slug === item.slug);
    const sizesClasses = showSizes ? 'active' : '';
    return (
      <div className={Styles.product}>
        {/* <div className={Styles.sale}>
          15% OFF
        </div> */}
        <div className={Styles.quickLinks}>
          {
            isInWishlist
              ? (
                <Tooltip
                  title="Remove from Wishlist"
                  placement="right"
                  classes={classes}
                  onClick={this.handleRemoveWishlist}
                >
                  <RedHeartIcon style={redIconStyles} />
                </Tooltip>
              )
              : (
                <Tooltip
                  title="Add to Wishlist"
                  placement="right"
                  classes={classes}
                  onClick={this.handleWishlistClick}
                >
                  <HeartIcon style={iconStyles} />
                </Tooltip>
              )
          }
          <Tooltip title="Add to Cart" placement="right" classes={classes} onClick={this.showSizes}>
            <CartIcon style={iconStyles} />
          </Tooltip>
        </div>
        <div className={Styles.productImage}>
          <div className={classnames(Styles.sizesOverlay, sizesClasses)}>
            <p>Select a size</p>
            <button className={Styles.sizeButton} onClick={() => { this.quickAddToCart({ name: 'S', slug: 'xs' }); }}>S</button>
            <button className={Styles.sizeButton} onClick={() => { this.quickAddToCart({ name: 'L', slug: 'l' }); }}>L</button>
            <button className={Styles.sizeButton} onClick={() => { this.quickAddToCart({ name: 'XL', slug: 'xl' }); }}>XL</button>
            <CloseIcon style={addToCartCloseIconStyles} onClick={this.closeSizes} />
          </div>
          <div className="lazyPlaceholder" style={{ paddingBottom: `${paddingBottom}%` }}>
            <NavLink to={`/product/${item.slug}`}>
              <img src={item.img.url} alt="" />
            </NavLink>
          </div>
        </div>
        <div className={Styles.productDetails}>
          <NavLink to={`/product/${item.slug}`}>
            <p>{item.product_name}</p>
            <p><b>Rs. {item.price}</b></p>
          </NavLink>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  item: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(tooltipClasses)(Item);
