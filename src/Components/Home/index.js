import React, { Component } from 'react';

import BannerSlider from '../BannerSlider';
import NewArrivals from '../NewArrivals';
import ItemsYouMayLike from '../ItemsYouMayLike';
import Offers from '../OffersSlider';
import CatLinks from '../CatLinks';
import FeaturedBrands from '../FeaturedBrands';
import Trending from '../Trending';
import InstagramSection from '../InstagramSection';

class Home extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div id="page">
        <BannerSlider />
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <NewArrivals />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <ItemsYouMayLike />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <Offers />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <CatLinks />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <FeaturedBrands />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <Trending />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <InstagramSection />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
