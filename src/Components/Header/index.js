import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import withSizes from 'react-sizes';
import PropTypes from 'prop-types';
import axios from 'axios';
import Drawer from '@material-ui/core/Drawer';
import MenuIcon from '@material-ui/icons/Menu';
import { connect } from 'react-redux';
import _ from 'underscore';

import MultiColumnList from '../Layout/MultiColumnList';
import MobileMenu from '../Layout/MobileMenu';
import CartHeader from '../Cart/CartHeader';
import Login from '../Login';
import UserButton from './UserButton';
import { mobileBreakPoint, apiUrl } from '../../globalSettings';

import styles from './header.module.scss';
import logo from '../../Static/img/labels_logo.png';

const mapStateToProps = state => (
  {
    isLoggedIn: state.isLoggedIn
  }
);

class Header extends Component {
  constructor() {
    super();
    this.state = {
      menuOffset: null,
      categories: [],
      brands: [],
      drawerActive: false
    };
    this.headerRef = React.createRef();
  }

  componentDidMount() {
    this.getCategories();
    this.getBrands();
  }

  menuRef = (elem) => {
    const { isMobile } = this.props;
    if (!isMobile) {
      this.setState({
        menuOffset: elem.getBoundingClientRect().x * -1
      });
    }
  }

  hideDrawer = () => {
    this.setState({
      drawerActive: false
    });
  }

  getCategories = () => {
    axios
      .get(`${apiUrl}/product_category`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            categories: response.data.response
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  getBrands = () => {
    axios
      .get(`${apiUrl}/brands`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            brands: response.data.response
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  render() {
    const {
      categories, brands, menuOffset, drawerActive
    } = this.state;
    const { isMobile, isLoggedIn } = this.props;
    const brandsChildren = brands.map((brand, index) => (
      <NavLink key={`brand-sub-${index}`} className={styles.subMenuLink} to={`/brand/${brand.brand_slug}`}>{brand.brand_name}</NavLink>
    ));
    const menCategory = !_.isEmpty(categories) && _.findWhere(categories, { category_slug: 'men' });
    const menChildren = !_.isEmpty(menCategory) && menCategory.children.map((child, index) => (
      <NavLink
        key={`nav-sub-link-${index}`}
        className={styles.subMenuLink}
        to={`/shop/men?pc=${child.category_slug}`}
      >
        {child.category_name}
      </NavLink>
    ));
    const womenCategory = !_.isEmpty(categories) && _.findWhere(categories, { category_slug: 'women' });
    const womenChildren = !_.isEmpty(womenCategory) && womenCategory.children
      .map((child, index) => (
        <NavLink
          key={`nav-sub-link-${index}`}
          className={styles.subMenuLink}
          to={`/shop/women?pc=${child.category_slug}`}
        >
          {child.category_name}
        </NavLink>
      ));
    return (
      <div id="header-wrapper" ref={this.headerRef} className="mui-fixed">
        <Drawer open={drawerActive} onClose={this.hideDrawer}>
          <MobileMenu
            logo={logo}
            menuItems={categories}
          />
        </Drawer>
        <div className="container cust-container">
          <div className={styles.top}>
            <div className="row">
              <div className="col __verticalAlign">
                <NavLink to="/"><img src={logo} alt="LABELS" /></NavLink>
              </div>
              <div className="col">
                <div className={styles.right}>
                  <NavLink to="/shop/sale" className={classnames('btn __red', styles.salebtn)}>Sale</NavLink>
                  <div className={styles.usermenu}>
                    <CartHeader />
                    {
                      isLoggedIn
                        ? <UserButton />
                        : <Login />
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.bottom}>
          <div className="container cust-container">
            <div className="row">
              <div className="col">
                {
                  isMobile
                    ? (
                      <div className={styles.drawerToggle}>
                        <MenuIcon onClick={() => { this.setState({ drawerActive: true }); }} />
                      </div>
                    )
                    : ''
                }
              </div>
              <div className="col">
                {
                  !isMobile
                    ? (
                      <ul className={styles.menu} ref={this.menuRef}>
                        <li>
                          <NavLink to="/shop/men" exact>Men</NavLink>
                          <div
                            className={styles.subMenu}
                            style={{
                              marginLeft: menuOffset,
                              width: `${this.headerRef.current && this.headerRef.current.clientWidth}px`
                            }}
                          >
                            <div className={styles.subMenuInner}>
                              <MultiColumnList
                                columns={3}
                                gutter={200}
                                wrapperClass={styles.subMenuWrapper}
                                columnClass={styles.subMenuColumn}
                              >
                                { menChildren || [] }
                              </MultiColumnList>
                            </div>
                          </div>
                        </li>
                        <li>
                          <NavLink to="/shop/women" exact>Women</NavLink>
                          <div
                            className={styles.subMenu}
                            style={{
                              marginLeft: `${menuOffset}px`,
                              width: `${this.headerRef.current && this.headerRef.current.clientWidth}px`
                            }}
                          >
                            <div className={styles.subMenuInner}>
                              <MultiColumnList
                                columns={3}
                                gutter={200}
                                wrapperClass={styles.subMenuWrapper}
                                columnClass={styles.subMenuColumn}
                              >
                                { womenChildren || [] }
                              </MultiColumnList>
                            </div>
                          </div>
                        </li>
                        <li>
                          <span>Brands</span>
                          <div
                            className={styles.subMenu}
                            style={{
                              marginLeft: `${menuOffset}px`,
                              width: `${this.headerRef.current && this.headerRef.current.clientWidth}px`
                            }}
                          >
                            <div className={styles.subMenuInner}>
                              <MultiColumnList
                                columns={3}
                                gutter={200}
                                wrapperClass={styles.subMenuWrapper}
                                columnClass={styles.subMenuColumn}
                              >
                                { brandsChildren }
                              </MultiColumnList>
                            </div>
                          </div>
                        </li>
                      </ul>
                    )
                    : ''
                }
              </div>
              <div className="col">
                <div className={styles.formWrap}>
                  <span className="_icon __search" />
                  <form action="">
                    <input type="text" placeholder="Search" />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  isMobile: PropTypes.bool,
  isLoggedIn: PropTypes.bool.isRequired
};

Header.defaultProps = {
  isMobile: false
};

const mapSizesToProps = ({ width }) => ({
  isMobile: width < mobileBreakPoint
});

export default withSizes(mapSizesToProps)(withRouter(connect(mapStateToProps)(Header)));
