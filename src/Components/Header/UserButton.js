import React, { Component } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { NavLink } from 'react-router-dom';
// import AccountIcon from '@material-ui/icons/AccountCircleOutlined';
// import HistoryIcon from '@material-ui/icons/HistoryOutlined';
import ExitIcon from '@material-ui/icons/ExitToAppOutlined';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import styles from './header.module.scss';

const mapStateToProps = state => (
  {
    user: state.user
  }
);

class UserButton extends Component {
  state = {
    anchorEl: null
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const { user } = this.props;
    const userFirstName = user && user.first_name;
    return (
      <>
        <span
          className={styles.loginButton}
          aria-owns={anchorEl ? 'simple-menu' : undefined}
          aria-haspopup="true"
          role="button"
          tabIndex="0"
          onClick={this.handleClick}
        >
          <PersonIcon />
          <span className={styles.userName}>{userFirstName}</span>
        </span>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {/* <NavLink to="/account" className={styles.userMenuLink}>
            <MenuItem onClick={this.handleClose}>
              <AccountIcon style={{ fontSize: 20 }} />
              <span className={styles.userMenuText}>My Account</span>
            </MenuItem>
          </NavLink>
          <NavLink to="/history" className={styles.userMenuLink}>
            <MenuItem onClick={this.handleClose}>
              <HistoryIcon style={{ fontSize: 20 }} />
              <span className={styles.userMenuText}>Order History</span>
            </MenuItem>
          </NavLink> */}
          <NavLink to="/logout" className={styles.userMenuLink}>
            <MenuItem onClick={this.handleClose}>
              <ExitIcon style={{ fontSize: 20 }} />
              <span className={styles.userMenuText}>Logout</span>
            </MenuItem>
          </NavLink>
        </Menu>
      </>
    );
  }
}

UserButton.propTypes = {
  user: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(UserButton);
