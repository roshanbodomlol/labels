import React, { Component } from 'react';

import BrandSlider from './BrandSlider';
import NewArrivals from '../NewArrivals';
import ItemsYouMayLike from '../ItemsYouMayLike';
import Offers from '../OffersSlider';
import CatLinks from '../CatLinks';
import FeaturedBrands from '../FeaturedBrands';
import Trending from '../Trending';
import InstagramSection from '../InstagramSection';

import styles from './styles.module.scss';

import brandLogo from '../../Static/img/brand-adidas-logo.png';
import brandHeader from '../../Static/img/brand-adidas-header.png';

class Brands extends Component {
  constructor() {
    super();
    this.state = {
      brand: {}
    };
  }

  componentDidMount() {
    this.setState({
      brand: {
        logo: brandLogo,
        header: brandHeader
      }
    });
  }

  render() {
    const { brand } = this.state;
    return (
      <div id="page">
        <div className={styles.headerBack}>
          <div className="container cust-container">
            <div className="row">
              <div className="col">
                <div className={styles.header}>
                  <img src={brandLogo} alt="" className={styles.brandLogo} />
                  {
                    brand.header
                      ? (
                        <div className={styles.headerImage}>
                          <img src={brand.header} alt="" />
                        </div>
                      )
                      : ''
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        <BrandSlider />
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <NewArrivals />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <ItemsYouMayLike />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <Offers />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <CatLinks />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <FeaturedBrands />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <Trending />
            </div>
          </div>
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <InstagramSection />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Brands;
