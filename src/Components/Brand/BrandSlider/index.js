import React, { Component } from 'react';
import uuid from 'uuid';

import ReactAnimeSlider from '../../ReactAnimeSlider';

import slide3 from '../../../Static/img/slider/banner/canyon.jpg';
import slide1 from '../../../Static/img/slider/banner/herons.jpg';
import slide5 from '../../../Static/img/slider/banner/woods.jpg';

import styles from './styles.module.css';

class BrandSlider extends Component {
  constructor() {
    super();
    this.state = {
      sliders: []
    };
    this.bannerRatio = (1920 / 580);
  }

  componentDidMount() {
    this.setState({
      sliders: [
        {
          img: {
            url: slide3
          },
          name: 'all in</br>or</br>nothing',
          url: '/'
        },
        {
          img: {
            url: slide1
          },
          name: 'all in</br>or</br>nothing',
          url: '/'
        },
        {
          img: {
            url: slide5
          },
          name: 'all in</br>or</br>nothing',
          url: '/'
        }
      ]
    });
  }

  render() {
    const { sliders } = this.state;
    const allSlides = sliders.map(slide => (
      <div key={`banner-slider-${uuid.v4()}`} className="banner" style={{ background: `url('${slide.img.url}') 0px center` }}>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <div className="details">
                {
                  slide.name
                    ? (
                      <p
                        className={styles.title}
                        dangerouslySetInnerHTML={{ __html: slide.name }}
                      />
                    )
                    : ''
                }
                {
                  slide.url
                    ? (
                      <a href={slide.url} className="btn __dark">Shop</a>
                    )
                    : ''
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    ));
    return (
      <div id="banner-slider">
        <ReactAnimeSlider
          duration={750}
          height={600}
          easing="easeOutQuint"
          autoplay
          interval={3000}
          arrows={false}
          pauseOnHover
          ratio={this.bannerRatio}
        >
          {allSlides}
        </ReactAnimeSlider>
      </div>
    );
  }
}

export default BrandSlider;
