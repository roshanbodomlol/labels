import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Ratings from '../Layout/Ratings';

import styles from './styles.module.scss';

import img from '../../Static/img/user.jpg';
import labelsImg from '../../Static/img/labels_logo.png';

class Reviews extends Component {
  constructor() {
    super();
    this.state = {
      reviews: []
      // reviewsLoaded: false
    };
  }

  componentDidMount() {
    this.setState({
      reviews: [
        {
          id: 1,
          created_at: 'December 4, 2018',
          author: {
            name: 'John Doe',
            picture: {
              url: img,
              width: 90,
              height: 90
            }
          },
          body: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum hoc loco sumo verbis his eandem certe vim voluptatis Epicurum nosse quam ceteros.</p>',
          rating: '4',
          response: {
            body: '<p>Uterque enim summo bono fruitur, id est voluptate. Quod autem ratione actum est, id officium appellamus.</p><p>Quid est enim aliud esse versutum? Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat</p>'
          }
        },
        {
          id: 2,
          created_at: 'December 4, 2018',
          author: {
            name: 'John Doe',
            picture: {
              url: img,
              width: 90,
              height: 90
            }
          },
          body: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verum hoc loco sumo verbis his eandem certe vim voluptatis Epicurum nosse quam ceteros.</p>',
          rating: '4',
          response: {
            body: '<p>Uterque enim summo bono fruitur, id est voluptate. Quod autem ratione actum est, id officium appellamus.</p><p>Quid est enim aliud esse versutum? Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat</p>'
          }
        }
      ]
    });
  }

  render() {
    const { title } = this.props;
    const { reviews } = this.state;
    const reviewsJSX = reviews.map((review, index) => (
      <div className={styles.review} key={`review-${index}`}>
        <div className={styles.userImg}>
          <img src={review.author.picture.url} alt="" />
        </div>
        <div className={styles.body}>
          <div className={styles.header}>
            <div className={styles.title}>
              <h4>{review.author.name}</h4>
              <Ratings rating={review.rating} />
            </div>
            <div className={styles.date}>
              {review.created_at}
            </div>
          </div>
          <div className={styles.reviewBody} dangerouslySetInnerHTML={{ __html: review.body }} />
          <div className={styles.response}>
            <div className={styles.by}>
              <img src={labelsImg} alt="" />
              <span><i>Response from Adidas</i></span>
            </div>
            <div
              className={styles.responsebody}
              dangerouslySetInnerHTML={{ __html: review.response.body }}
            />
          </div>
        </div>
      </div>
    ));
    return (
      <div className={styles.wrapper}>
        <div className="row">
          <div className="col">
            <div className={styles.sectionTitle}>
              { title }
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 offset-sm-3">
            {reviewsJSX}
          </div>
        </div>
      </div>
    );
  }
}

Reviews.propTypes = {
  title: PropTypes.string
};

Reviews.defaultProps = {
  title: ''
};

export default Reviews;
