/* eslint-disable */
import React from 'react';
import {NavLink} from 'react-router-dom';

import OneColumnPage from '../OneColumnPage';

const FAQ = () => (
  <OneColumnPage>
    <h4>1. WHAT ARE LABEL&#39;s DELIVERY OPTIONS?</h4>
    <p>
      Levels orders are prepared, dispatched and conveyed Monday– Friday, barring national occasions holidays inside 3-21 business days.

Amid checkout, we&#39;ll furnish you with the assessed conveyance date dependent on your request&#39;s conveyance address.

Delivery charges are free for requests of Rs or more. A delivery charge of Rs
    </p>
    <h4>Extra INFORMATION</h4>

<p>Requests are prepared Monday-Friday, aside from national occasions. Know that requests may encounter longer preparing and
conveyance times amid occasions.</p>

<p>
  Requests can&#39;t be conveyed to PO boxes, reshippers or bundle sending administrations.
  Requests can&#39;t be conveyed to addresses outside the nation or area where they were put. Be that as it may, you can shop Level.com on
  various destinations all around
</p>

<h4>2. How do i return my products?</h4>
<p>
  At Levels, we make items that assist you perform better, play better and feel good. Regardless of whether you&#39;re utilizing our shoes in
  the city or the court, or our clothing in the studio or on the field, we need you to be happy with your buy.
  If your product is defective / damaged or incorrect/incomplete at the time of delivery, please contact us within the applicable return
  window. Your product may be eligible for refund depending on the product category and condition
  For more details see our <NavLink to="/return-policy">return policy</NavLink>
</p>

<h4>3. How long does it takes for the delivery?</h4>
<p>
  The time your request takes to be delivered varies relying upon the sort of items you request, your delivery address and the delivery
  technique you pick. Amid deal and advancement periods delivery may take somewhat longer than anticipated. The typical delivery
  period is 3 to 4 working days.
</p>

<h4>
  4. How do i create my account?
</h4>

<p>It&#39;s easy to create an account.</p>
<p>The initial step is to click &#39;Login&#39; on the upper right corner of the site, and afterward pick &#39;Make Account&#39; to begin the procedure.
</p>

<p>Once there you ought to enter your own data. That is your first and last name, and choosing on the off chance that you are beyond 13
years old.</p>

<p>Next you enter your &#39;login data&#39;. This is your email, and you have to make a secret key and affirm it. At that point pick your
correspondence inclinations, and make a point to acknowledge the <NavLink to="terms-and-conditions">Terms and Conditions</NavLink>.
Complete the procedure by clicking <b>&#39;Submit&#39;</b>.</p>

<h4>
  5. How do i make payment?
</h4>
<p>You can pay COD (Cash on delivery) or through our various online payment partner like eswa, khalti etc.
</p>
<h4>6. How do I earn label coins?</h4>
<ul>
  <li>In purchases upto Rs.5000, 5% of the total amount is earned.</li>
  <li>In purchases upto Rs.10000, 10% of the total amount is earned.</li>
  <li>In purchases above Rs.10000, 12% of the total amount is earned.</li>
  <li>In purchases of SALE items, 5% of the total amount is earned.</li>
  <li>The first time any account is registered 100 Label coins are awarded.</li>
</ul>

  </OneColumnPage>
);

export default FAQ;
