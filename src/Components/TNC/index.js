/* eslint-disable */
import React from 'react';

import OneColumnPage from '../OneColumnPage';

const TNC = () => (
  <OneColumnPage title="TERMS AND CONDITIONS">
    <h4>1. Introduction</h4>
<p>
  Welcome to www.labels.com hereby known as “Labels”, “we”, ”our” or “us”. We are an e-commerce
  platform and these are the terms and conditions governing your use of Labels and its other services. By
  using our website (hereinafter referred to as the “Site”), you accept the terms and conditions mentioned
  here and agree to comply with the terms and conditions (hereinafter referred to as “T&amp;C”). It is
  accepted that you agree to be bound by T&amp;C upon your use of the Site. If you do not agree to be bound
  by T&amp;C then please do not register with, access or use the Site.
  This site reserves the sole right to add, modify or remove parts of T&amp;C at any time without any prior
  notification. Please check these T&amp;C regularly for updates. Changes are effective when posted on the
  site. Your continued use of the Site after posting changes to T&amp;C will be taken as acceptance of those
  changes.
</p>
<h4>2. Conditions of use</h4>
<h4>A. Your Account</h4>
<p>
  We may require you to provide personal information for registering an account with us to be able to use
  the full range of services provided by us. We hold the sole right to validate and/or invalidate or ask for
  authentication to any account bearers without any prior notice and shall not be responsible for any loss
  or harm caused to anyone arising from such actions.
</p>
<p>
  The confidentiality of your account details, user identification and security details are your
  responsibility. By agreeing to our T&amp;C you agree to accept this responsibility and to take necessary steps
  to prevent the misuse of account at all times. You agree to notify us immediately if you have any reason
  to believe that someone else has access to your security details, your account has been breached or
  security of your details have been compromised. You agree that any use of the Site using your account
  shall be considered to be done or authorized by you. You agree to fully indemnify us against any and all
  losses arising from the use of or access to the Site through your account.
</p>
<p>
  You are responsible for providing us the correct and complete details at all times. You are required to
  update details on your account in real time through access or communicating via Customer Service
  channels to update us and you will be deemed responsible for any loss or harm occurred by the false or
  absence of information in your details. We reserve the sole right, without any questions asked to refuse
  access to the Site, edit or remove content or deactivate accounts at any time without any prior notice.
  We may ask you to update your details or invalidate the account in the absence of it without providing
  any prior notice or reason and shall not be responsible for any loss caused by or suffered by you. You
  hereby agree to be responsible for the confidentiality of your account and change your password
  regularly and liable for any unauthorized use of your username and/or password.
</p>

<h4>B. Privacy</h4>
<p>
  Please go through our Privacy Policy, where we have stated our commitment to securing your privacy.
  Any personal data submitted by you or collected by us during your Site visit shall be treated as strictly
  confidential, as per the Privacy Policy and prevailing laws and regulations. If you have any objections to
  the collection, usage and storage of your personal information as stated in the Privacy Policy, please
  refrain from using the Site.
</p>
<h4>C. Continued Availability of the Site</h4>
<p>We will do our best to ensure that the Site is constantly available, accessible uninterrupted and error-
free at all times. Your access to the Site may be suspended or restricted to allow for maintenance,
upgrading, or introduction of new features, facilities without any prior notice. We will try our best to
make these transition phases as short as possible. We cannot guarantee the quality of your service due
to the nature of the Site or quality of your Internet service but we will consider attempting to make it
better and convenient for you.</p>
<h4>D. License to access the Site</h4>
<p>
  By accessing the site you confirm that you are at least 18 years of age or are under parent/guardian
  supervision. We grant you a non-transferable, revocable and non-exclusive right to use the site as per
  the T&amp;C described hereby for the purpose of shopping, browsing through our list of products to be sold
  on our site. No person or business may register as a member of the Site more than once. Any breach of
  these terms, if identified will lead to revoking of the license granted.
</p>
<p>
  Contents presented on the Site are the representation of actual products for informational purposes
  including price, features, uses and other details and cannot be guaranteed as completely accurate by us.
  Submissions or opinions expressed on this site are those of the individual(s) posting such content and
  may not reflect our opinions as a brand.
</p>
<p>
  We grant you full access to use the site as described in T&amp;C but not to download, modify or manipulate
  the Site or any portion of it in any manner. You are not authorized to resell or make commercial use of
  the site or its contents; any derivative use of the Site or its contents, any collection and use of any
  products listings, description or prices; any downloading or copying of account information for the
  benefit of another seller; or any use of data mining, extraction tools or robots.
  This Site or any portion of it (including but not limited to any copyrighted material, trademarks, or other
  proprietary information) may not be reproduced, duplicated, copied, sold, resold, visited, distributed or
  otherwise exploited for any commercial purpose without express written consent by us as may be
  applicable.
</p>
<p>
  You agree and undertake not to perform restricted activities listed within this section; undertaking
  these activities will result in immediate cancellation of your account, services, reviews, orders or any
  existing incomplete transaction with us and in severe punishment as applicable by the laws.
</p>
<h4>
  E. Communication Platform
</h4>
<p>
  It is agreed and understood that this Site is an e-commerce platform to purchase products listed at the
  price mentioned there from any place at any time using the payment method that’s convenient for you.
  You agree and understand that we are facilitators on the site and cannot control any transactions on the
  Site or any payment gateway as made available to you by an independent service provider.
</p>
<h4>F. Feedback and Information</h4>
<p>
  Any feedback you provide to this Site shall be deemed to be non-confidential. Labels shall be free to use
  such information on an unrestricted basis. Further, by submitting the feedback, You represent and
  warrant that (i) Your feedback does not contain confidential or proprietary information about you or
  about third parties; (ii) Labels is not under any obligation of confidentiality, express or implied, with
  respect to the feedback; (iii) Labels may have something similar to the feedback already under
  consideration or in development, and (iv) You are not entitled to any compensation or reimbursement
  of any kind from Labels for the feedback under any circumstances.
</p>
<p>
  To clarify, Labels may at its discretion, make any modifications or changes to the Website, Content
  and/or Services on the basis of such feedback, however, Labels shall not be obliged to do so. Further, in
  the event that Labels makes any changes or modifications to the Website, Content and/or Services on
  the basis of any such feedback, You shall not have any rights or title (including any intellectual property
  rights) in such changes or modifications to the Website, Content and/or Services. You expressly waive
  any and all rights in such changes or modifications to the Website, Content and/or Services, and assign
  to Labels, all worldwide rights and title (including any intellectual property rights) to such changes or
  modifications to the Website, Content and/or Services, in perpetuity.
</p>

<h4>G. Your Conduct</h4>
<p>
  You must not use the website in any way that causes, or is likely to cause, the Site or access to it to be
  interrupted, damaged or impaired in any way. You must not engage in activities that could harm or
  potentially harm the Site, its employees, officers, representatives, stakeholders or any other party
  directly or indirectly associated with the Site or access to it to be interrupted, damaged or impaired in
  any way. You understand that you, and not us, are responsible for all electronic communications and
  content sent from your computer to us and you must use the Site for lawful purposes only. You are
  strictly prohibited from using the Site for fraudulent purposes, or in connection with a criminal offense
  or other unlawful activity to send, use or reuse any material that does not belong to you; or is illegal,
  offensive (including but not limited to material that is sexually explicit content or which promotes
  racism, bigotry, hatred or physical harm), deceptive, misleading, abusive, indecent, harassing,
  blasphemous, defamatory, obscene, pornographic, pedophilic or menacing; ethnically objectionable,
  disparaging or in breach of copyright, trademark, confidentiality, privacy or any other proprietary
  information or right; or is otherwise injurious to third parties; or relates to or promotes money
  laundering or gambling; or is harmful to minors in any way; or impersonates another person; or
  threatens the unity, integrity, security or sovereignty of Nepal or friendly relations with foreign States;
  or objectionable or otherwise unlawful in any manner whatsoever; or which consists of or contains
  software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any
  &quot;spam” Use the Site for illegal purposes to cause annoyance, inconvenience or needless anxiety for any
  other purposes that is other than what is intended by us.
</p>
<h4>
  H. Claims against Contents
</h4>
<p>
  If you believe any of the contents posted on our Site is illegal, offensive, sexually explicit, sexist, racist,
  contents that promotes bigotry, hatred, physical harm; promotes money laundering, gambling, ,
  deceptive, misleading, pornographic, indecent, harassing, blasphemous, ethnically objectionable,
  obscene; is harmful to third parties; threatens the unity, integrity, security or sovereignty of Nepal or
  friendly relations with friendly states; objectionable or otherwise unlawful, immoral, unethical content,
  please notify us immediately by writing a mail to us. We will attempt to investigate and remove valid
  objectionable within a reasonable amount of time.
</p>
<p>
  As a brand, we stand for the rights to intellectual property of every individual. If you believe any of our
  content is close to infringement of anyone’s creative content, please write to us and we will undertake
  every practical endeavor to address your concern within a reasonable amount of time.
  Please be sure to provide your name, address, contact information and as many relevant details of the
  claim including all the parties involved, the issue you are trying to raise in details with proofs, amongst
  others.
</p>
<h4>
  I. Trademarks and Copyrights
</h4>
<p>
  Any trademarks used on our Site (including but not limited to www.labels.com, Labels and Labels Logo)
  are registered trademarks in the relevant jurisdiction(s). Any graphics, logos, page headers and button
  icons are trademarked and cannot be used elsewhere that might create confusion among customers,
  harms our brand image or discredits us. Other trademarks available in our site are the property of
  respective owners who may or may not be associated or sponsored by us.
</p>
<p>
  All intellectual property rights, whether registered or unregistered, in the Site, information content on
  the Site and all the website design, including, but not limited to text, graphics, software, photos, video,
  music, sound, and their selection and arrangement, and all software compilations, underlying source
  code, and software shall remain our property. The entire contents of the Site also are protected by
  copyright as a collective work under Nepali copyright laws and international conventions. All rights are
  reserved.
</p>
<h4>
  J. Losses
</h4>

<p>
  We will not be responsible for any business or personal losses (including but not limited to loss of
  profits, revenue, contracts, anticipated savings or data) or any indirect loss that cannot be reasonably
  foreseen by both you and us.
</p>
<h4>
  K. Amendments to Conditions or Alterations of service
</h4>
<p>
  We reserve the sole right to add, amend or omit parts of the Site, its Policy, T&amp;C, services, and promises
  at any time as deemed appropriate by us without any prior notice. You will be subjected to these
  changes to policies and T&amp;C once you use the Site after policies and T&amp;C are updated. If any government
  authority brings in changes to the policies or laws of the land, the changes are effective as per the
  conditions stated by the authority.
</p>
<h4>
  L. Events beyond our control
</h4>
<p>
  We shall not be held responsible for any delay or failure to comply with our promises or obligations if it
  arises from any causes beyond our reasonable control. These conditions, however, will not affect your
  statutory rights.
</p>
<h4>
  M. Waiver
</h4>
<p>
  You acknowledge and recognize that we are a private commercial enterprise and reserve the right to
  conduct business to achieve our objectives in a manner we deem fit. You also acknowledge that if you
  breach the conditions stated on our Site and we take no action, we are still entitled to use our rights and
  remedies in any other situation where you breach these conditions.
</p>
<h4>
  N. Termination
</h4>
<p>
  We may revoke any or all of your rights granted under T&amp;C, terminate the T&amp;C without any prior notice
  to you. You shall immediately cease all access to and use of the Site and we shall immediately revoke all
  passwords and account identification issued to you and deny your access to and use of this Site in whole
  or in part. Any termination of this agreement shall not affect the respective rights and obligations
  (including payment obligations) of the parties arising before the date of termination. You agree and
  understand that the Site shall not be liable to you or any other person as a result of such termination. If
  you are dissatisfied with the Site or with any T&amp;C, rules, policies or practices in operating the Site, your
  remedy is to discontinue using the Site.
</p>

<h4>
  O. Loyalty Programs
</h4>
<p>
  You will be rewarded with Loyalty Points (hereafter referred to as “Points”) as a reward to our loyal
  customers. The Points can be used while doing transactions with us and will not be valid anywhere else
  except at Labels. The distribution of Points shall be done through various programs and offers. We shall
  hold the sole right to issue, distribute, validate and invalidate the Points in any way we see fit.
</p>

<h4>
  P. Reselling Label Products
</h4>
<p>
  Reselling Label products is strictly prohibited. If any unauthorized personnel or business is found
  committing the above act, legal action can be taken against him/her.
</p>
  </OneColumnPage>
);

export default TNC;
