import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';

import { userLogout } from '../../Store/actions';

const mapDispatchToProps = dispatch => (
  {
    userLogout: item => dispatch(userLogout(item))
  }
);

class Logout extends Component {
  componentDidMount() {
    // localStorage.removeItem('lbl_token');
    Cookie.remove('lbl_token', { path: '/' });
    window.location.replace('/');
  }

  render() {
    return (
      <div className="logout">
        logout
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Logout);
