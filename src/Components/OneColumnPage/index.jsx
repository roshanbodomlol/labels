import React from 'react';
import PropTypes from 'prop-types';

import styles from './page.module.scss';

const OneColumnPage = ({ children, title }) => (
  <div className={styles.oneColumn}>
    <div id="main" className="general-content">
      <div className="container container-md">
        <div className="row">
          <div className="col">
            <div className={styles.heading}>
              {title}
            </div>
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
);

OneColumnPage.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string
};

OneColumnPage.defaultProps = {
  children: '',
  title: ''
};

export default OneColumnPage;
