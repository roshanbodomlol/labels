import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import _ from 'underscore';
import moment from 'moment';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import PersonIcon from '@material-ui/icons/Person';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CalendarIcon from '@material-ui/icons/CalendarTodayOutlined';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import red from '@material-ui/core/colors/red';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';

import Form, { Input } from '../Forms';
import { userLogin, openLoginDialog, closeLoginDialog } from '../../Store/actions';
import Button from '../Layout/Button';
import { apiUrl, tokenExpireAfterDays } from '../../globalSettings';
import labelsLogo from '../../Static/img/labels_logo.png';

import styles from './styles.module.scss';

const loginStyles = {
  paper: {
    width: 485,
    borderRadius: 0
  },
  label: {
    fontSize: 12
  }
};

const mapDispatchToProps = dispatch => (
  {
    userLogin: user => dispatch(userLogin(user)),
    openLoginDialog: () => dispatch(openLoginDialog()),
    closeLoginDialog: () => dispatch(closeLoginDialog())
  }
);

const mapStateToProps = state => (
  {
    isLoginDialogOpen: state.isLoginDialogOpen
  }
);

class Login extends Component {
  constructor() {
    super();
    this.state = {
      show: 'login',
      selectedDate: undefined,
      registerEmail: undefined,
      registerPassword: undefined,
      registerConfirmPassword: undefined,
      registerFirstName: undefined,
      registerLastName: undefined,
      registerNumber: undefined,
      loginName: undefined,
      loginPassword: undefined,
      loading: false,
      rememberMe: false,
      errorSnackOpen: false,
      errorMessage: '',
      successSnackOpen: false,
      successMessage: '',
      errors: {}
    };
    this.pickerDialogRef = React.createRef();
  }

  closeDialog = () => {
    const { closeLoginDialog: closeDialog } = this.props;
    closeDialog();
  }

  openDialog = () => {
    const { openLoginDialog: openDialog } = this.props;
    openDialog();
  }

  clearForm = () => {
    this.setState({
      selectedDate: undefined,
      registerEmail: undefined,
      registerPassword: undefined,
      registerConfirmPassword: undefined,
      registerFirstName: undefined,
      registerLastName: undefined,
      registerNumber: undefined,
      loginName: undefined,
      loginPassword: undefined
    });
  }

  handleDateChange = (date) => {
    this.setState({ selectedDate: date });
  };

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  setActiveView = (view) => {
    this.setState({ show: view });
  }

  handleRemeberMe = (e) => {
    this.setState({
      rememberMe: e.target.checked
    });
  }

  handleLogin = (event) => {
    const { userLogin: userLoginProp } = this.props;
    const { rememberMe } = this.state;
    event.preventDefault();
    this.setState({
      loading: true
    });
    const {
      loginName,
      loginPassword
    } = this.state;
    if (loginName && loginPassword) {
      const loginBody = new FormData();
      loginBody.set('username', loginName);
      loginBody.set('password', loginPassword);
      axios({
        url: `${apiUrl}/members/login`,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        data: loginBody
      })
        .then((response) => {
          if (response.data.type === 'success') {
            this.closeDialog();
            this.clearForm();
            this.setState({
              loading: false
            });
            // localStorage.setItem('lbl_token', response.data.response.token);
            if (rememberMe) {
              Cookie.set('lbl_token', response.data.response.identifier, { expires: tokenExpireAfterDays, path: '/' });
            } else {
              Cookie.set('lbl_token', response.data.response.identifier, { expires: 1, path: '/' });
            }
            userLoginProp(response.data.response);
          } else {
            this.closeDialog();
            this.clearForm();
            this.setState({
              loading: false,
              errorSnackOpen: true,
              errorMessage: response.data.message
            });
          }
        })
        .catch((e) => {
          console.log(e);
          this.closeDialog();
          this.clearForm();
        });
    } else {
      this.setState({ loading: false });
    }
  }

  handleRegister = (event) => {
    const { userLogin: userLoginProp } = this.props;
    event.preventDefault();
    this.setState({
      loading: true
    });
    const {
      selectedDate,
      registerEmail,
      registerPassword,
      registerConfirmPassword,
      registerFirstName,
      registerLastName,
      registerNumber
    } = this.state;
    if (selectedDate && registerEmail && registerEmail && registerPassword && registerConfirmPassword && registerFirstName && registerLastName && registerNumber) { // eslint-disable-line
      const registerBody = new FormData();
      registerBody.set('first_name', registerFirstName);
      registerBody.set('last_name', registerLastName);
      registerBody.set('email', registerEmail);
      registerBody.set('dob', moment(selectedDate).format('YYYY-MM-DD'));
      registerBody.set('mobile', registerNumber);
      registerBody.set('password', registerPassword);
      registerBody.set('passconf', registerConfirmPassword);
      axios({
        url: `${apiUrl}/members/create`,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        data: registerBody
      })
        .then((response) => {
          if (response.data.type === 'success') {
            this.setState({ loading: false });
            Cookie.set('lbl_token', response.data.response.identifier, { expires: 1, path: '/' });
            userLoginProp(response.data.response);
            this.closeDialog();
            this.clearForm();
            this.setState({
              successSnackOpen: true,
              successMessage: response.data.message
            });
          } else {
            this.setState({
              loading: false,
              errors: response.data.message
            });
          }
        })
        .catch((e) => {
          console.log(e);
          this.closeDialog();
          this.clearForm();
        });
    } else {
      this.setState({ loading: false });
      console.log('Internal Error');
    }
  }

  closeErrorSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ errorSnackOpen: false });
  }

  checkError = (field) => {
    const { errors } = this.state;
    const fieldError = _.property(field)(errors);
    return fieldError && (
      <div className="with-error">
        { fieldError }
      </div>
    );
  }

  render() {
    const {
      show,
      selectedDate,
      loading,
      registerEmail,
      registerPassword,
      registerConfirmPassword,
      registerFirstName,
      registerLastName,
      registerNumber,
      loginName,
      loginPassword,
      errorSnackOpen,
      errorMessage,
      successSnackOpen,
      successMessage,
      errors
    } = this.state;
    const { classes, isLoginDialogOpen } = this.props;
    return (
      <React.Fragment>
        <span className={styles.loginButton}>
          <PersonIcon onClick={this.openDialog} />
        </span>
        <Snackbar
          key="login-component-error-snack"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={errorSnackOpen}
          autoHideDuration={4500}
          onClose={this.closeErrorSnack}
          style={{
            marginTop: 15
          }}
        >
          <SnackbarContent
            style={{
              background: red[500]
            }}
            message={<span style={{ fontFamily: 'tex' }}>{errorMessage}</span>}
            action={[
              <CloseIcon
                style={{
                  cursor: 'pointer'
                }}
                onClick={this.closeErrorSnack}
              />
            ]}
          />
        </Snackbar>
        <Snackbar
          key="login-component-success-snack"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={successSnackOpen}
          autoHideDuration={4500}
          onClose={this.closeSuccessSnack}
          style={{
            marginTop: 15
          }}
        >
          <SnackbarContent
            style={{
              background: red[500]
            }}
            message={<span style={{ fontFamily: 'tex' }}>{successMessage}</span>}
            action={[
              <CloseIcon
                style={{
                  cursor: 'pointer'
                }}
                onClick={this.closeSuccessSnack}
              />
            ]}
          />
        </Snackbar>
        <Dialog
          open={isLoginDialogOpen}
          onClose={this.closeDialog}
          classes={{ paper: classes.paper }}
        >
          <div className={styles.modalInner}>
            <div className={styles.header}>
              <CloseIcon onClick={this.closeDialog} style={{ cursor: 'pointer' }} />
            </div>
            <div className={styles.logo}>
              <img src={labelsLogo} alt="" />
            </div>
            {
              show === 'login'
                ? (
                  <div className={styles.loginForm}>
                    <div className={styles.formWrap}>
                      <form className="labels-form" onSubmit={(event) => { this.handleLogin(event); }}>
                        <div className="control-row">
                          <div className="input-wrap">
                            <input
                              autoFocus // eslint-disable-line
                              type="text"
                              placeholder="Email"
                              value={loginName}
                              required
                              onChange={(e) => { this.setState({ loginName: e.target.value }); }}
                            />
                          </div>
                        </div>
                        <div className="control-row">
                          <div className="input-wrap">
                            <input
                              type="password"
                              placeholder="Password"
                              value={loginPassword}
                              required
                              onChange={(e) => {
                                this.setState({ loginPassword: e.target.value });
                              }}
                            />
                          </div>
                        </div>
                        <div className="control-row d-flex justify-content-between">
                          <FormGroup>
                            <FormControlLabel
                              classes={{ label: classes.label }}
                              control={(
                                <Checkbox
                                  color="primary"
                                  icon={<CheckBoxOutlineBlankIcon style={{ fontSize: 16 }} fontSize="inherit" />}
                                  checkedIcon={<CheckBoxIcon style={{ fontSize: 16 }} fontSize="inherit" />}
                                  value="rememberYes"
                                  onClick={(e) => { this.handleRemeberMe(e); }}
                                />
                              )}
                              label="Remember Me"
                            />
                          </FormGroup>
                          <NavLink className={styles.forgotLink} to="/">Forgot Password</NavLink>
                        </div>
                        <div className="control-row d-flex justify-content-center">
                          <Button
                            size="medium"
                            type="submit"
                            mode="primary"
                            loading={loading}
                            onClick={this.handleLogin}
                          >
                            LOG IN
                          </Button>
                        </div>
                        <div className="control-row d-flex justify-content-center">
                          <Button
                            size="medium"
                            type="button"
                            mode="primary"
                            onClick={() => { this.setActiveView('register'); }}
                          >
                            SIGN UP
                          </Button>
                        </div>
                      </form>
                    </div>
                  </div>
                )
                : (
                  <div className={styles.registerForm}>
                    <div className={styles.formWrap}>
                      <Form onSubmit={(event) => { this.handleRegister(event); }}>
                        <div className="control-row">
                          <Input
                            type="text"
                            placeholder="First Name"
                            required
                            value={registerFirstName}
                            name="first_name"
                            errors={errors}
                            onChange={(e) => {
                              this.setState({ registerFirstName: e.target.value });
                            }}
                          />
                        </div>
                        <div className="control-row">
                          <Input
                            type="text"
                            name="last_name"
                            placeholder="Last Name"
                            required
                            value={registerLastName}
                            errors={errors}
                            onChange={(e) => {
                              this.setState({ registerLastName: e.target.value });
                            }}
                          />
                        </div>
                        <div className="control-row">
                          <Input
                            onClick={this.openPickerDialog}
                            name="dob"
                            type="text"
                            placeholder="Date Of Birth"
                            required
                            errors={errors}
                            value={selectedDate && moment(selectedDate).format('Do MMM YYYY')}
                            other={(
                              <div className={styles.calendarIcon}>
                                <CalendarIcon
                                  style={{
                                    position: 'absolute',
                                    right: 6,
                                    top: 13,
                                    color: '#DADADA',
                                    pointerEvents: 'none'
                                  }}
                                />
                              </div>
                            )}
                          />
                        </div>
                        <div className="control-row">
                          <Input
                            type="email"
                            placeholder="Email"
                            name="email"
                            errors={errors}
                            required
                            value={registerEmail}
                            onChange={(e) => {
                              this.setState({ registerEmail: e.target.value });
                            }}
                          />
                        </div>
                        <div className="control-row">
                          <Input
                            type="tel"
                            placeholder="Phone Number"
                            name="mobile"
                            errors={errors}
                            required
                            value={registerNumber}
                            onChange={(e) => {
                              this.setState({ registerNumber: e.target.value });
                            }}
                          />
                        </div>
                        <div className="control-row">
                          <Input
                            type="password"
                            placeholder="Password"
                            name="password"
                            errors={errors}
                            help="Password should be 6-12 characters long and
                               have at least one number and at least one Capital
                                letter"
                            required
                            value={registerPassword}
                            onChange={(e) => {
                              this.setState({ registerPassword: e.target.value });
                            }}
                          />
                        </div>
                        <div className="control-row">
                          <Input
                            type="password"
                            placeholder="Confirm Password"
                            name="passconf"
                            errors={errors}
                            required
                            value={registerConfirmPassword}
                            onChange={(e) => {
                              this.setState({ registerConfirmPassword: e.target.value });
                            }}
                          />
                          <small>* All fields are mandatory</small>
                        </div>
                        <div className={styles.hide}>
                          <MuiPickersUtilsProvider utils={MomentUtils}>
                            <DatePicker
                              ref={this.pickerDialogRef}
                              margin="normal"
                              label="Date Of Birth"
                              value={selectedDate}
                              onChange={this.handleDateChange}
                              style={{
                                width: '100%'
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </div>
                        <br />
                        <div className="control-row d-flex justify-content-center">
                          <Button
                            size="medium"
                            type="submit"
                            mode="primary"
                            loading={loading}
                            onClick={this.handleRegister}
                          >
                            CREATE ACCOUNT
                          </Button>
                        </div>
                        <div className="control-row d-flex justify-content-center">
                          <Button
                            size="medium"
                            type="submit"
                            mode="primary"
                            onClick={() => { this.setActiveView('login'); }}
                          >
                            LOG IN
                          </Button>
                        </div>
                      </Form>
                    </div>
                  </div>
                )
            }
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  userLogin: PropTypes.func.isRequired,
  closeLoginDialog: PropTypes.func.isRequired,
  openLoginDialog: PropTypes.func.isRequired,
  isLoginDialogOpen: PropTypes.bool.isRequired
};

const LoginComp = connect(mapStateToProps, mapDispatchToProps)(withStyles(loginStyles)(Login));
export default LoginComp;
