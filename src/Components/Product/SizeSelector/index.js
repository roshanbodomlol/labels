import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './styles.module.scss';

class SizeSelector extends Component {
  constructor() {
    super();
    this.state = {
      active: false
    };
  }

  activate = () => {
    this.setState({
      active: true
    });
    setTimeout(() => {
      window.addEventListener('click', this.deActivate);
    }, 100);
  }

  deActivate = () => {
    this.setState({
      active: false
    });
    window.removeEventListener('click', this.deActivate);
  }

  render() {
    const { activeSize, sizes, updateSize } = this.props;
    const { active } = this.state;
    const sizeClass = active ? cn(styles.wrapper, 'active') : styles.wrapper;
    const sizeButtons = sizes.map((size, index) => (
      <button
        type="button"
        key={`size-btn-${index}`}
        className={activeSize && activeSize.slug === size.slug ? 'active' : ''}
        onClick={() => { updateSize(size); }}
      >
        { size.name }
      </button>
    ));
    return (
      <div className={sizeClass}>
        <div className={styles.heading} onClick={this.activate} role="button" tabIndex="-1">
          <span>
            Select Size
          </span>
          {
            activeSize && `: ${activeSize.name}`
          }
          <span className={cn(styles.icon, '__icon')} />
        </div>
        <div className={styles.options}>
          <div className={styles.inner}>
            { sizeButtons }
          </div>
        </div>
      </div>
    );
  }
}

SizeSelector.propTypes = {
  activeSize: PropTypes.object,
  sizes: PropTypes.arrayOf(PropTypes.object),
  updateSize: PropTypes.func.isRequired
};

SizeSelector.defaultProps = {
  activeSize: null,
  sizes: []
};

export default SizeSelector;
