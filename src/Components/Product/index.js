import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import PrevArrowIcon from '@material-ui/icons/KeyboardArrowUp';
import NextArrowIcon from '@material-ui/icons/KeyboardArrowDown';
// import { NavLink } from 'react-router-dom';
import axios from 'axios';

import Wrapper, { PlaceholderDiv } from '../LoadingPlaceholder';
import ReactAnimeSlider from '../ReactAnimeSlider';
import SizeSelector from './SizeSelector';
import Selector from '../Layout/Selector';
import ThumbnailList from '../Layout/ThumbnailListHorizontal';
import AddToCart from '../Cart/AddToCart';

import styles from './styles.module.scss';

import { quantityLimit, apiUrl } from '../../globalSettings';

class Product extends Component {
  constructor() {
    super();
    this.state = {
      loaded: false,
      name: '',
      images: [],
      activeImage: 0,
      productQuantity: 1,
      slug: null,
      id: null,
      price: null,
      activeSize: null,
      relatedProducts: [],
      sizes: [
        {
          name: 'XS',
          slug: 'xs'
        },
        {
          name: 'S',
          slug: 's'
        },
        {
          name: 'M',
          slug: 'm'
        },
        {
          name: 'L',
          slug: 'l'
        },
        {
          name: 'XL',
          slug: 'xl'
        }
      ]
    };
    this.productImageRatio = 1.511450381679389;
    this.navSliderSettings = {
      dots: false,
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      vertical: true,
      verticalSwiping: false,
      prevArrow: <PrevArrowIcon />,
      nextArrow: <NextArrowIcon />,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            vertical: false
          }
        }
      ]
    };
    this.imageSlidingRef = false;
  }

  componentDidMount() {
    this.getProduct();
  }

  componentDidUpdate(prevProps) {
    const { match } = prevProps;
    const { match: prevMatch } = this.props;
    if (prevMatch.params.slug !== match.params.slug) {
      this.getProduct();
    }
  }

  getProduct = () => {
    this.setState({ loaded: false });
    const { match } = this.props;
    axios
      .get(`${apiUrl}/products/getSingleProduct/${match.params.slug}`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            name: response.data.response.product_name,
            slug: response.data.response.slug,
            id: response.data.response.product_id,
            price: parseInt(response.data.response.price, 10),
            images: [
              response.data.response.feature_image,
              ...response.data.response.gallery_images
            ],
            relatedProducts: response.data.response.related,
            loaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  updateProductQuantity = (productQuantity) => {
    this.setState({
      productQuantity
    });
  }

  updateProductSize = (activeSize) => {
    this.setState({
      activeSize
    });
  }

  updateImageSlide = (activeImage) => {
    if (!this.imageSlidingRef) {
      this.setState({
        activeImage
      });
    }
  }

  render() {
    const {
      slug,
      id,
      name,
      loaded,
      images,
      sizes,
      productQuantity,
      activeSize,
      activeImage,
      price,
      relatedProducts
    } = this.state;
    const slides = [];
    const navSlides = [];
    for (let i = 0; i < images.length; i++) {
      slides.push(<div key={`product-slider-${i}`} className={styles.productSlide} style={{ background: `url('${images[i].main.url}')` }} />);
      navSlides.push(
        <img
          key={`product-slider-nav-${i}`}
          role="button"
          tabIndex="-1"
          onClick={() => { this.updateImageSlide(i); }}
          src={images[i].thumb.url}
          alt=""
          className={activeImage === i ? 'active' : ''}
        />
      );
    }
    const quantityOptions = [];
    for (let i = 1; i <= quantityLimit; i++) {
      quantityOptions.push({
        name: i,
        value: i
      });
    }
    return (
      <div id="main">
        <div className="container cust-container">
          {/* <div className={styles.breadcrumbs}>
            <ul>
              <li><NavLink to="/">Home</NavLink></li>
              <li><NavLink to="/shop/men">Men</NavLink></li>
              <li><NavLink to="/shop/men&c=1">Shoes</NavLink></li>
            </ul>
          </div> */}
        </div>
        <div className={styles.productWrapper}>
          {
            loaded
              ? (
                <>
                  <div className={styles.productSliderWrapper}>
                    <div className={styles.productSliderNav}>
                      <div className="container cust-container">
                        <div className="row">
                          <div className="col-md-8">
                            <div className={styles.productVisuals}>
                              <div className={styles.navSliderWrap}>
                                <div className={styles.navSlider}>
                                  <Slider {...this.navSliderSettings}>
                                    {navSlides}
                                  </Slider>
                                </div>
                              </div>
                              <div className={styles.productSlider}>
                                <ReactAnimeSlider
                                  duration={750}
                                  height={600}
                                  easing="easeOutQuint"
                                  autoplay={false}
                                  interval={3000}
                                  arrows={false}
                                  pauseOnHover
                                  ratio={this.productImageRatio}
                                  activeSlide={activeImage}
                                  update={
                                    (animating) => {
                                      this.imageSlidingRef = animating;
                                    }}
                                >
                                  {slides}
                                </ReactAnimeSlider>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-4">
                            <div className={styles.productDetails}>
                              <div className={styles.productName}>
                                {name}
                              </div>
                              <span>Rs. {price}</span>
                              <div className={styles.highlights}>
                                <div className={styles.subTitle}>PRODUCT HIGHLIGHTS</div>
                                <ul>
                                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                     Non igitur de improbo, sed de callido improbo quaerimus
                                  </li>
                                  <li>Ut id aliis narrare gestiant</li>
                                  <li>Dici enim nihil potest verius.
                                     Quare, quoniam de primis naturae
                                      commodis satis dietum est nunc de maioribus
                                  </li>
                                  <li>Atqui eorum nihil est eius generis,
                                     ut sit in fine atque extrerno bonorum
                                  </li>
                                  <li>Videmusne ut pueri ne verberibus quidem
                                     a contemplandis rebus perquirendisque deterreantur
                                  </li>
                                </ul>
                              </div>
                              <div className={styles.productOptions}>
                                <div className={styles.productOptionsLeft}>
                                  <SizeSelector
                                    sizes={sizes}
                                    activeSize={activeSize}
                                    updateSize={(size) => { this.updateProductSize(size); }}
                                  />
                                </div>
                                <div className={styles.productOptionsRight}>
                                  <Selector
                                    options={quantityOptions}
                                    updateSelect={(option) => {
                                      this.updateProductQuantity(option.value);
                                    }}
                                    selected={{ value: productQuantity, name: productQuantity }}
                                  />
                                </div>
                              </div>
                              <div className={styles.productOptions}>
                                <div className={styles.productOptionsLeft}>
                                  <div className={styles.addToCart}>
                                    {
                                      slug
                                        ? (
                                          <AddToCart
                                            slug={slug}
                                            size={activeSize}
                                            quantity={productQuantity}
                                            price={price}
                                            id={id}
                                          />
                                        )
                                        : ''
                                    }
                                  </div>
                                </div>
                                <div className={styles.productOptionsRight} />
                              </div>
                              <a className={styles.policy} href="/">read our return policy statement</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="container cust-container">
                    <div className="row">
                      <div className="col-md-8">
                        <div className={styles.description}>
                          <div className={styles.sectionTitle}>
                            DESCRIPTION
                          </div>
                          <p>Ut id aliis narrare gestiant?
                           Dici enim nihil potest verius. Quare, quoniam
                           de primis naturae commodis satis dietum est nunc
                            de maioribus consequentibusque videamus.
                             Ergo adhuc, quantum equidem intellego,
                              causa non videtur fuisse mutandi nominis.
                               Atqui eorum nihil est eius generis, ut sit in
                              fine atque extrerno bonorum. Videmusne ut pueri
                               ne verberibus quidem a contemplandis rebus
                                perquirendisque deterreantur?
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  </>
              )
              : (
                <Wrapper>
                  <PlaceholderDiv height={500}>
                    Product Placeholder
                  </PlaceholderDiv>
                </Wrapper>
              )
          }
        </div>
        <div className="container cust-container">
          <div className="row">
            <div className="col">
              <ThumbnailList title="Related Products" listItems={relatedProducts} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Product.propTypes = {
  match: PropTypes.object.isRequired // eslint-disable-line
};

export default Product;
