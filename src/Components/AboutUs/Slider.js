import React, { Component } from 'react';
import Slider from 'react-slick';

import Styles from './styles.module.scss';

// Images
import plane from '../../Static/img/plane.jpg';
import bear from '../../Static/img/bearboy.jpg';

class AboutSlider extends Component {
  state = {
    slider: []
  };

  componentDidMount() {
    this.setState(
      {
        slider: [
          {
            image: plane
          },
          {
            image: bear
          }
        ]
      }
    );
  }

  render() {
    const { slider } = this.state;
    const slides = slider.map(slide => (
      <div className={Styles.aboutSlider}>
        <img src={slide.image} alt="" />
        <div className="cust-container">
          <div className={Styles.details}>
            <h1>Unwrap Inspiration</h1>
            <p>
              Fitbit motivates you to reach your health and fitness goals
              by tracking your activity, exercise, sleep, weight and more.
            </p>
          </div>
        </div>
      </div>
    ));
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    };
    return (
      <Slider {...settings}>
        {slides}
      </Slider>
    );
  }
}

export default AboutSlider;
