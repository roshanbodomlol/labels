import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';
import AboutSlider from './Slider';

import Styles from './styles.module.scss';

import yellow from '../../Static/img/yellow.png';

const AboutUs = () => (
  <div id="main">
    <AboutSlider />
    <div className="container">
      <div className={Styles.aboutExcerpt}>
        <h4>About Labels</h4>
        <p>
          It was 18 years ago that five like-minded individuals whowere very
          enthusiastic about brands and quality clothing came together to
          bring a new culture to Nepal. All of the partners who were also
          good friends came from different backgrounds; branded retail business,
          academics, sports etc and this is what helped to form a balanced
          partnership of education andexperience in the company. They opened
          their very first multi-brand store called GQ inNew Road and this
          store running till date.
        </p>
      </div>
    </div>
    <div className={Styles.greyWrapper}>
      <div className={Styles.midContainer}>
        <div className={Styles.contentWrapper}>
          <div className="row">
            <div className="col-sm-6">
              <div className={Styles.content}>
                <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>
                <p>
                  And from this store, they started bringing inoriginal
                  Adidas products and this is how the
                  journey of ‘Labels’ began.
                  <br /> <br />
                  As the name suggests, ‘Labels’ is all about bringing the desired
                  and quality brands (labels) under one roof by catering to the
                  mass and giving them a premium shopping experience.
                  <br /> <br />
                  ‘Labels is a well-known name when it comes to brands and it is
                  due to our consistent effort to bring the original products in
                  the market that we have very loyal and supportive customers. It
                  is also because of this that our customers are growing by the day.
                  If it were not for our customers, then the store would not be as
                  popular and consistent until today. Since our customers are our
                  utmost priority, we want to show them our gratitude and
                  appreciation by giving them a great consumer experience.
                </p>
                <NavLink to="/about-us-more">Learn more</NavLink>
              </div>
            </div>
            <div className="col-sm-6">
              <img src={yellow} alt="" />
            </div>
          </div>
        </div>

        <div className={Styles.mapSection}>
          <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>
          <p>
            Here at levels, we are dedicated to being a pioneer in the e-commerce
            market, bringing all the big name brands under one roof and making it
            accessible to everyone in Nepal at their doorstep. To learn more about
            business at Levels keep up to date on the latest products, athletes
            features and on all things at Levels website and Level app. We are
            committed to providing genuine quality service to every corner of Nepal.
            ‘Labels’ which was established by Roots Fashion Pvt. Ltd. Roots Fashion,
            which was registered in 2001, has a total of nine stores all over
            Kathmandu and has become a household name when it comes to brands as
            it is the sole distributor of Adidas, Reebok, Pepe, Levi’s, Cube Bikes,
            Rockport and Dr Martens in Nepal.
          </p>
          <div className={Styles.map}>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.219985677243!2d85.31566971458321!3d27.710493331961526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1950f35261a7%3A0xef03b0a3de1c71ba!2sLabels!5e0!3m2!1sen!2snp!4v1553249612301" title="map" width="100%" height="450" frameBorder="0" allowFullscreen />
          </div>
        </div>

        <div className={Styles.contentWrapper}>
          <div className="row">
            <div className="col-sm-6">
              <img src={yellow} alt="" />
            </div>
            <div className="col-sm-6">
              <div className={classNames(Styles.content, Styles.contentLeft)}>
                <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>
                <p>
                  If you have the passion for premium fashion and sports brands
                  and have the hunger to change the current scenario of Nepal
                  to the next level then our job openings might be the right fit
                  for you. Here are some of the hot jobs at Levels.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className={Styles.contentWrapper}>
          <div className="row">
            <div className="col-sm-6">
              <div className={Styles.content}>
                <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>
                <p>
                  Levels aim to be a brand with a purpose that moves Nepal forward.
                  To achieve that, we support form all member of our team to
                  understand and deliver on our goals – from our leaders to the
                  manager, to the employees in our stores, and workers. A strong
                  foundation for governance, coupled with a sustainability mindset,
                  provides the motive for driving collective decision making and
                  accountability across the company. The corporate responsibility
                  committee of the company is responsible for reviewing Lebel&apos;
                  significant strategies, activities, and policies regarding
                  sustainability, community impact and charitable activities,
                  among other duties, and make various recommendations. The
                  committee sets the quality for sustainability within the
                  Level’s business strategy
                  <br /> <br />
                  The corporate responsibility committee of the company is
                  responsible for reviewing Lebel&apos; significant strategies,
                  activities, and policies regarding sustainability, community
                  impact and charitable activities, among other duties, and make
                  various recommendations. The committee sets the quality for
                  sustainability within the Level’s business strategy.
                </p>
              </div>
            </div>
            <div className="col-sm-6">
              <img src={yellow} alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AboutUs;
