import React, { Component } from 'react';
import uuid from 'uuid';

import styles from './styles.module.scss';

import adidas from '../../Static/img/brands/adidas.png';
import reebok from '../../Static/img/brands/reebok.png';
import levis from '../../Static/img/brands/levis.png';
import fitbit from '../../Static/img/brands/fitbit.png';

class FeaturedBrands extends Component {
  constructor() {
    super();
    this.state = {
      brands: []
    };
  }

  componentDidMount() {
    this.setState({
      brands: [
        {
          name: 'adidas',
          img: adidas,
          url: '/'
        },
        {
          name: 'reebok',
          img: reebok,
          url: '/'
        },
        {
          name: 'levis',
          img: levis,
          url: '/'
        },
        {
          name: 'fitbit',
          img: fitbit,
          url: '/'
        }
      ]
    });
  }

  render() {
    const { brands } = this.state;
    const allBrands = brands.map(brand => (
      <div key={`brand-list-${uuid.v4()}`} className={styles.brand}>
        <img src={brand.img} alt="" />
      </div>
    ));
    return (
      <div className={styles.brandList}>
        <div className="row">
          <div className="col">
            <div className={styles.title}>FEATURED BRANDS</div>
          </div>
        </div>
        <div className={styles.brandWrap}>
          {allBrands}
        </div>
      </div>
    );
  }
}

export default FeaturedBrands;
