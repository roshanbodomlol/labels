/* eslint-disable */
import React from 'react';

import OneColumnPage from '../OneColumnPage';

const LabelCoins = () => (
  <OneColumnPage title="Label coins">
<p>
  Shop with labels and earn the Label Coins as rewards. Label Coins are points that you earn for being the customer of Levels as well as
shopping with Labels. Some of the major highlights of the label coins are as below:
</p>
<ul>
  <li>How to earn Label Coins?
  <ul>
    <li>In purchases up to Rs , 5% of the total amount is earned.</li>
    <li>In purchases up to Rs.1, 10% of the total amount is earned.</li>
    <li>In purchases above Rs., 12% of the total amount is earned.</li>
    <li>In purchases of SALE items, 5% of the total amount is earned.</li>
    <li>The first time an account is registered 100 Label coins is awarded.</li>
  </ul>
  </li>
</ul>
<li>Anytime a user recommends the app/site, he will be able to give a referral code to his friends or family. If this referral code is
used he earns 100 Label coins.</li>
<li>Every time a user reviews an item, they earn 50 Label coins. There is no limit to how many items they can review but there is a
limit of 150 Label coins per month that you can earn through reviews.</li>
<li>
  Label coins are redeemable only through purchases online and NOT in-store.
</li>
<li>
  Label coins will expire after a year if not used.
</li>
<li>Label coins are non-transferable.</li>
<li>There will be a routine quiz (daily/weekly), where one selected user with the right answer will earn 50 Label coins.</li>
  </OneColumnPage>
);

export default LabelCoins;
