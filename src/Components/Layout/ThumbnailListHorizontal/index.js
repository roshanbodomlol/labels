import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import LazyLoad from 'react-lazy-load';
import withSizes from 'react-sizes';
import { NavLink } from 'react-router-dom';
import Slider from 'react-slick';
import _ from 'underscore';

import styles from './styles.module.scss';

const mobileSliderSettings = {
  autoplay: false,
  arrows: false,
  dots: true,
  slidesToShow: 2,
  slidesToScroll: 1,
  infinite: false,
  responsive: [
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1
      }
    }
  ]
};

const ThumbnailListHorizontal = ({ title, listItems, isMobile }) => {
  let list = [];
  let placeHolderCols = [];
  if (listItems.length < 5) {
    placeHolderCols = _.range(5 - listItems.length).map((item, i) => (
      <div key={`placeHolderCols-${i}`} className="col" />
    ));
  }
  if (listItems) {
    const limitedItems = listItems.slice(0, 5);
    if (isMobile) {
      list = limitedItems.map(listItem => (
        <div className="__mobileSlider" key={`list-item-${uuid.v4()}`}>
          <NavLink to={`/product/${listItem.slug}`}>
            <div className={styles.item}>
              <img src={listItem.img.url} width={listItem.img.width} height={listItem.img.height} alt="" />
            </div>
          </NavLink>
        </div>
      ));
    } else {
      list = limitedItems.map((listItem) => {
        const thumbnailRatio = listItem.img.width / listItem.img.height;
        const paddingBottom = (1 / thumbnailRatio) * 100;
        return (
          <div className="col" key={`list-item-${uuid.v4()}`}>
            <NavLink to={`/product/${listItem.slug}`}>
              <div className={styles.item}>
                <div className="lazyPlaceholder" style={{ paddingBottom: `${paddingBottom}%` }}>
                  <LazyLoad>
                    <img src={listItem.img.url} width={listItem.img.width} height={listItem.img.height} alt="" />
                  </LazyLoad>
                </div>
              </div>
            </NavLink>
          </div>
        );
      });
    }
  }
  return (
    <div className={styles.listWrap}>
      <div className="row">
        <div className="col">
          <div className={styles.title}>
            {title}
          </div>
        </div>
      </div>
      <div className="row">
        {
          isMobile
            ? (
              <div className="col">
                <div className={styles.mobileHorizontalSlider}>
                  <Slider {...mobileSliderSettings}>
                    {list}
                  </Slider>
                </div>
              </div>
            )
            : [
              list,
              placeHolderCols
            ]
        }
      </div>
    </div>
  );
};

ThumbnailListHorizontal.propTypes = {
  listItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  title: PropTypes.string.isRequired,
  isMobile: PropTypes.bool.isRequired
};

const mapSizesToProps = ({ width }) => ({
  isMobile: width < 769
});

export default withSizes(mapSizesToProps)(ThumbnailListHorizontal);
