import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import ThreeDots from '../../ThreeDots';

const Button = ({
  children, classes, size, type, mode, loading, onClick
}) => {
  const buttonClasses = classnames({
    btn: true,
    classes,
    loading,
    __medium: size === 'medium',
    __full: size === 'full',
    __small: size === 'small',
    __red: mode === 'primary',
    __link: mode === 'link'
  });
  return (
    <>
      {
        loading
          ? <button className={buttonClasses} disabled><ThreeDots variant="flashing" /></button>
          : (
            <button
              className={buttonClasses}
              type={type}
              onClick={onClick}
            >
              {children}
            </button>
          )
      }
    </>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.string,
  size: PropTypes.oneOf(['medium', 'full', 'small']),
  mode: PropTypes.oneOf(['primary', 'link']),
  loading: PropTypes.bool,
  type: PropTypes.oneOf(['submit', 'button', 'reset']),
  onClick: PropTypes.func
};

Button.defaultProps = {
  children: '',
  classes: '',
  size: 'medium',
  mode: 'primary',
  loading: false,
  type: 'button',
  onClick: () => {}
};

export default Button;
