import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import cn from 'classnames';

import styles from './styles.module.css';

const MultiColumnList = ({
  children, columns, gutter, wrapperClass, columnClass
}) => {
  const output = [];
  for (let i = 0; i < columns; i++) {
    const column = children.filter((child, index) => index % columns === i);
    const marginRightPX = i !== columns - 1
      ? `${gutter}px`
      : '0px';
    output.push(
      <div
        key={`mutlilist-${uuid.v4()}`}
        className={cn(styles.column, columnClass)}
        style={{ marginRight: marginRightPX }}
      >
        {column}
      </div>
    );
  }
  return (
    <div className={cn(styles.multiList, wrapperClass)}>
      {output}
    </div>
  );
};

MultiColumnList.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  columns: PropTypes.number,
  gutter: PropTypes.number,
  wrapperClass: PropTypes.string,
  columnClass: PropTypes.string
};

MultiColumnList.defaultProps = {
  columns: 3,
  gutter: 15,
  wrapperClass: 'multi-wrapper',
  columnClass: 'multi-column'
};

export default MultiColumnList;
