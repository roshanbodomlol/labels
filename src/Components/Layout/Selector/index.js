import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import _ from 'underscore';

import styles from './styles.module.scss';

class Selector extends Component {
  constructor() {
    super();
    this.state = {
      selectActive: false
    };
  }

  hideSelect = (e) => {
    if (e.target.className !== 'sortBtn') {
      this.setState({
        selectActive: false
      });
      window.removeEventListener('click', this.hideSelect);
    }
  };

  hideSelectForce = () => {
    this.setState({
      selectActive: false
    });
    window.removeEventListener('click', this.hideSelect);
  }

  handleSelect = () => {
    const { selectActive } = this.state;
    if (!selectActive) {
      this.setState({
        selectActive: true
      });
      setTimeout(() => {
        window.addEventListener('click', this.hideSelect);
      }, 100);
    }
  }

  render() {
    const {
      options, updateSelect, selected, title
    } = this.props;
    const { selectActive } = this.state;
    const selectOptions = options.map((option, index) => (
      <div className={styles.option} key={`select-option-${index}`}>
        <button
          className="sortBtn"
          onClick={() => {
            updateSelect(option);
            this.hideSelectForce();
          }}
        >
          {option.name}
        </button>
      </div>
    ));
    const activeOption = _.findWhere(options, selected);
    return (
      <div className={cn(styles.selector, 'selector')}>
        <div
          className={cn(styles.activeOption, 'active-option')}
          onClick={this.handleSelect}
          role="button"
          tabIndex="-1"
        >
          <span>{title}</span>
          <span className={cn(styles.activeOptionRight, 'activeOptionRight')}>
            {activeOption.name}
            <span className={cn(styles.icon, '__icon')} />
          </span>
        </div>
        <div
          className={
            selectActive
              ? cn(styles.otherOptions, styles.active)
              : styles.otherOptions}
        >
          {selectOptions}
        </div>
      </div>
    );
  }
}

Selector.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  updateSelect: PropTypes.func.isRequired,
  selected: PropTypes.object.isRequired,
  title: PropTypes.string
};

Selector.defaultProps = {
  title: ''
};

export default Selector;
