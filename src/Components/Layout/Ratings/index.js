import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const Ratings = ({ rating }) => {
  const className = cn(styles.star, `__${rating}`);
  return (
    <div className={className} />
  );
};

Ratings.propTypes = {
  rating: PropTypes.string
};

Ratings.defaultProps = {
  rating: '5'
};

export default Ratings;
