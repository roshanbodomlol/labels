import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

const ColumnList = ({ children, cols }) => {
  const colClass = `col-md-${12 / cols} col-sm-6`;
  const list = children.map(child => (
    <div key={`col-list-item-${uuid.v4()}`} className={colClass}>
      {child}
    </div>
  ));
  return (
    <div className="row">
      {list}
    </div>
  );
};

ColumnList.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  cols: PropTypes.number
};

ColumnList.defaultProps = {
  cols: 4
};

export default ColumnList;
