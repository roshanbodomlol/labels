import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.module.scss';

export const BlockTitle = ({ primary, secondary, action }) => (
  <div className={styles.block}>
    <div className={styles.head}>
      {
        primary
          ? (
            <div className={styles.titlePrimary}>
              {primary}
            </div>
          )
          : (
            <div className={styles.titleSecondary}>
              {secondary}
            </div>
          )
      }
      {
        action
          ? (
            <div className={styles.action}>
              {action}
            </div>
          )
          : ''
      }
    </div>
  </div>
);

export const BlockBody = ({
  primary, secondary, children, noMargin
}) => (
  <div className={classnames(styles.block, 'layout-block', noMargin ? styles.noMargin : '')}>
    <div className={styles.head}>
      {
        primary
          ? (
            <div className={styles.titlePrimary}>
              {primary}
            </div>
          )
          : (
            <div className={styles.titleSecondary}>
              {secondary}
            </div>
          )
      }
    </div>
    <div className={styles.body}>
      {children}
    </div>
  </div>
);

BlockTitle.propTypes = {
  primary: PropTypes.element,
  secondary: PropTypes.element,
  action: PropTypes.element
};

BlockTitle.defaultProps = {
  action: null,
  primary: null,
  secondary: null
};

BlockBody.propTypes = {
  primary: PropTypes.element,
  secondary: PropTypes.element,
  children: PropTypes.node,
  noMargin: PropTypes.bool
};

BlockBody.defaultProps = {
  children: null,
  primary: null,
  secondary: null,
  noMargin: false
};

export default BlockTitle;
