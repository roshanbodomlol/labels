import React, { Component } from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';

import MenuItem from './MenuItem';

import styles from './styles.module.scss';

class MobileMenu extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { logo, menuItems } = this.props;
    const items = menuItems.length > 0 && menuItems.map((item, index) => (
      <MenuItem key={`drawer-menu--item-${index}`} item={item} />
    ));
    return (
      <div className={styles.wrapper}>
        <div className={styles.main}>
          {
            logo
              ? (
                <React.Fragment>
                  <div className={styles.header}>
                    <img src={logo} alt="" />
                  </div>
                  <div className={styles.body}>
                    <List>
                      {items}
                    </List>
                  </div>
                </React.Fragment>
              )
              : ''
          }
        </div>
      </div>
    );
  }
}

MobileMenu.propTypes = {
  logo: PropTypes.string,
  menuItems: PropTypes.arrayOf(PropTypes.object).isRequired
};

MobileMenu.defaultProps = {
  logo: null
};

export default MobileMenu;
