import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

const styles = {
  li: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  button: {
    background: 'none',
    border: 'none'
  },
  link: {
    color: '#000',
    fontFamily: 'tex'
  },
  subLi: {
    paddingLeft: '30px',
    fontSize: '14px'
  }
};

class MenuItem extends Component {
  constructor() {
    super();
    this.state = {
      expand: false
    };
  }

  render() {
    const { item, classes } = this.props;
    const { expand } = this.state;
    const children = item.children && item.children.map((child, index) => (
      <ListItem button key={`drawer-menu-sub-${index}`} className={classes.subLi}>
        <NavLink className={classes.link} to={`/shop/${item.category_slug}?pc=${child.category_slug}`}>{child.category_name}</NavLink>
      </ListItem>
    ));
    return (
      <React.Fragment>
        <ListItem className={classes.li}>
          <NavLink className={classes.link} to={`/shop/${item.category_slug}`}>{item.category_name}</NavLink>
          {
            item.children
              ? (
                <button className={classes.button}>
                  {
                    expand
                      ? (
                        <ExpandLess
                          onClick={() => { this.setState({ expand: !expand }); }}
                        />
                      )
                      : (
                        <ExpandMore
                          onClick={() => { this.setState({ expand: !expand }); }}
                        />
                      )
                  }
                </button>
              )
              : ''
          }
        </ListItem>
        {
          item.children
            ? (
              <Collapse in={expand} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {children}
                </List>
              </Collapse>
            )
            : ''
        }
      </React.Fragment>
    );
  }
}

MenuItem.propTypes = {
  item: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MenuItem);
