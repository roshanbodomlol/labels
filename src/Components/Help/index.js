/* eslint-disable */
import React from 'react';
import {NavLink} from 'react-router-dom';

import OneColumnPage from '../OneColumnPage';

const Help = () => (
  <OneColumnPage>
    <h4>1. track order</h4>
    <p>While shopping with Levels, we provide you the premium facility to track your order until the delivery of the product reaches your doorstep.</p>
    <h4>2. Delivery & returns</h4>

    <p>If your product is defective/damaged or incorrect/incomplete at the time of delivery, please contact us within the applicable return window. Your product may be eligible for a refund depending on the product category and condition. </p>
    <p>For more details see our <NavLink to="/policy">Return Policy</NavLink></p>

    <h4>3. Premier delivery</h4>
    <p>
      Premier Delivery gives you free delivery for just Rs 1000 a year.
It's available for all Nepali customers; however, the below delivery address are not included:
Daang, mustang.....

    </p>
    <p>
      Premier Delivery is valid on the order you make on Levels and can take up to 30 minutes to be activated against your account. You'll receive For Rs 1000 per year, you’ll get free unlimited Standard, Next Day and Delivery (saving up to Rs 100 per order)*.

    </p>
    <p>Sign Up now to grab these great benefits from Levels.</p>
    <h4>4. 10% Student discount</h4>
    <p>About the Levels Student Discount</p>
    <p>Unlock 10% Student Discount at Levels with valid student ID registered with your account.</p>
    <p>Unlock 10% Student Discount at Levels with valid student ID registered with your account.</p>
    <h4>Terms and conditions</h4>
    <ol>
      <li>Can only be considered for products on the Levels website</li>
      <li>Can only be considered for products on the Levels website</li>
      <li>Charges for delivery will be calculated after the code has been applied unless a Free Delivery promotion is used.</li>
    </ol>
  </OneColumnPage>
);

export default Help;
