import React from 'react';
import PropTypes from 'prop-types';
import { openLoginDialog } from '../../Store/actions';
import store from '../../Store';

const LoginLocal = ({ classes }) => (
  <button
    type="button"
    className={classes}
    onClick={() => { store.dispatch(openLoginDialog()); }}
  >
    Login
  </button>
);

LoginLocal.propTypes = {
  classes: PropTypes.string
};

LoginLocal.defaultProps = {
  classes: ''
};

export default LoginLocal;
