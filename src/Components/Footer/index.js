import React from 'react';
import { NavLink } from 'react-router-dom';
import cn from 'classnames';

import styles from './styles.module.scss';

const Footer = () => (
  <div className={styles.footer}>
    <div className="container cust-container">
      <div className="row">
        <div className="col-sm-2">
          <span>HELP</span>
          <ul className={styles.footerList}>
            <li>
              <NavLink to="/help">Help</NavLink>
            </li>
            <li>
              <NavLink to="/faq">FAQ</NavLink>
            </li>
          </ul>
        </div>
        <div className="col-sm-2">
          <span>About Labels</span>
          <ul className={styles.footerList}>
            <li>
              <NavLink to="/about-us">About Us</NavLink>
            </li>
            <li>
              <NavLink to="/career">Career at Labels</NavLink>
            </li>
            <li>
              <NavLink to="/corporate">Corporate</NavLink>
            </li>
            <li>
              <NavLink to="/responsibility">Responsibility</NavLink>
            </li>
          </ul>
        </div>
        <div className="col-sm-2">
          <span>More From Labels</span>
          <ul className={styles.footerList}>
            <li>
              <NavLink to="/mobile-apps">Mobile and Labels App</NavLink>
            </li>
            <li>
              <NavLink to="/gift-vouchers">Gift Vouchers</NavLink>
            </li>
          </ul>
        </div>
        <div className="col-sm-2">
          <span>Legal</span>
          <ul className={styles.footerList}>
            <li>
              <NavLink to="/privacy-cookies">Privacy & Cookies</NavLink>
            </li>
            <li>
              <NavLink to="/terms-and-conditions">Ts & Cs</NavLink>
            </li>
            <li>
              <NavLink to="/accessibility">Accessibility</NavLink>
            </li>
            <li>
              <NavLink to="/sitemap">Sitemap</NavLink>
            </li>
            <li>
              <NavLink to="/label-coins">Label Coins</NavLink>
            </li>
          </ul>
        </div>
        <div className="col-sm-4">
          <div className={cn(styles.footerList, styles.socialList)}>
            <span>Follow Us</span>
            <ul className={styles.social}>
              <li>
                <a href="/" target="_blank" rel="nofollow noreferrer">
                  <span className="_icon __fb" />
                </a>
              </li>
              <li>
                <a href="/" target="_blank" rel="nofollow noreferrer">
                  <span className="_icon __insta" />
                </a>
              </li>
              <li>
                <a href="/" target="_blank" rel="nofollow noreferrer">
                  <span className="_icon __tw" />
                </a>
              </li>
              <li>
                <a href="/" target="_blank" rel="nofollow noreferrer">
                  <span className="_icon __ut" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Footer;
