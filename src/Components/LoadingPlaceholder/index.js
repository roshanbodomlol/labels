import React from 'react';
import PropTypes from 'prop-types';

import styles from './placeholder.module.scss';

export const PlaceholderDiv = ({ children, height }) => (
  <div
    className={styles.placeholderDiv}
    style={{ height: `${height}px` }}
  >
    {children}
  </div>
);

export const PlaceholderSpan = ({ children }) => (
  <div className={styles.placeholderSpan}>
    {children}
  </div>
);

export const PlaceholderFlex = ({ children }) => (
  <div className={styles.placeholderFlex}>
    {children}
  </div>
);

const Wrapper = ({ children }) => (
  <div className={styles.wrapper}>
    {children}
  </div>
);

PlaceholderDiv.propTypes = {
  children: PropTypes.string,
  height: PropTypes.number
};

PlaceholderDiv.defaultProps = {
  height: 30,
  children: ''
};

PlaceholderSpan.propTypes = {
  children: PropTypes.string.isRequired
};

PlaceholderFlex.propTypes = {
  children: PropTypes.string.isRequired
};

Wrapper.propTypes = {
  children: PropTypes.element.isRequired
};

export default Wrapper;
