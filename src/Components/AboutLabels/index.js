import React from 'react';
import Styles from './styles.module.scss';

// import back from '../../img/back.png';

const About = () => (
  <div>
    <div className={Styles.about}>
      <div className="cust-container">
        <div className={Styles.details}>
          <h1>About Labels</h1>
          <p>
            Labels is a well-known name when it comes to brands and it is due
            to our consistent effort to bring the original products in the market
          </p>
        </div>
      </div>
    </div>
    <div className="container">
      <div className={Styles.content}>
        <h4>About Labels</h4>
        <p>
          It was 18 years ago that five like-minded individuals who were very
          enthusiastic about brands and quality clothing came together to bring
          a new culture to Nepal. All of the partners who were also good friends
          came from different backgrounds; branded retail business,academics,
          sports etc and this is what helped to form a balanced partnership of
          education andexperience in the company. They opened their very first
          multi-brand store called GQ inNew Road and this store running till date.
          <br /> <br />
          And from this store, they started bringing inoriginal Adidas products
          and this is how the journey of ‘Labels’ began. As the name suggests,
          ‘Labels’ is all about bringing the desired and quality brands (labels)
          under one roof by catering to the mass and giving them a premium
          shopping experience.
          <br /> <br />
          ‘Labels&apos; is a well-known name when it comes to brands and it is due to
           our consistent effort to bring the original products in the market
           that we have very loyal and supportive customers. It is also because
           of this that our customers are growing by the day. If it were not for
           our customers, then the store would not be as popular and consistent
           until today. Since our customers are our utmost priority, we want to
           show them our gratitude and appreciation by giving them a great
           consumer experience.
          <br /> <br />
           We are also aware of the fast-changing times and we would also like to
           cater to a newer group of audience, the tech-savvy youngsters by
           connection through the official Labels App and Labels Website.
          <br /> <br />
          ‘Labels’ was established by Roots Fashion Pvt. Ltd. Labels is you answer
          if you are into branded clothes and shoes. Roots Fashion, which was
          registered in 2001, has a total of nine stores all over Kathmandu and
          has become a household name when it comes to brands as it is the sole
          distributor of Adidas, Reebok, Pepe, Levi’s, Cube Bikes, Rockport
          and Dr Martens in Nepal.
          <br /> <br />
          In the journey of these fantastic 18 years, we have come a long way and
          we still have a long way to go. For us at Labels, our long term goal is
          to become the number 1 branded retail giant of Nepal by being our best.
          <br /> <br />
          At present, we have about 15 physical stores all around Nepal as mentioned here:
        </p>
        <ol>
          <li>Adidas: Durbar Marg</li>
          <li>Reebok: Durbar Marg (Sherpa Mall)</li>
          <li>Levis: Durbar Marg</li>
          <li>Adidas: Patan</li>
          <li>Adidas: Bluebird Mall, Tripureshwor</li>
          <li>Labels: Bluebird Mal, Tripureshwor</li>
          <li>Adidas: NSCC, Putalisadak</li>
          <li>Roots DEsign: NSCC Putalisadak</li>
          <li>GQ: Newroad (Multi branded Store)</li>
          <li>Adidas: City Center</li>
          <li>Levi’s : City Center</li>
          <li>Adidas: Civil Mall</li>
          <li>Reebok: Patan (Coming Soon)</li>
          <li>Adidas: Pokhara</li>
        </ol>
        <p>
          To enhance the shopping experience for our customers, We have officially
          launched the Levels online store and Level phone app that can be downloaded
          for free from Google play store or apple app store for free. Now you can shop
          at Labels anytime from anywhere. Now Levels is a pioneer in the e-commerce market,
          bringing all the big name brands under one roof and making it accessible
          to everyone in Nepal at their doorstep.
          <br /> <br />
          Through the label mobile app and label website, not only can you be
          updated with the latest arrivals of apparel in different outlets but
          also enjoy discounts and earn loyalty points.
        </p>
      </div>
    </div>
  </div>
);

export default About;
