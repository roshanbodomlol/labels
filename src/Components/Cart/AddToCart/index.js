import React, { Component } from 'react';
import _ from 'underscore';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';

import { addToCart } from '../../../Store/actions';
import styles from './styles.module.scss';

import { quantityLimit } from '../../../globalSettings';

const snackStyles = {
  message: {
    fontFamily: 'tex'
  },
  snackBar: {
    marginTop: '15px'
  },
  snackError: {
    background: red[500]
  },
  closeIcon: {
    cursor: 'pointer'
  }
};

const mapDispatchToProps = dispatch => (
  {
    addToCart: item => dispatch(addToCart(item))
  }
);

const mapStateToProps = state => (
  {
    cart: state.cart
  }
);

class ConnectedAddToCart extends Component {
  constructor() {
    super();
    this.state = {
      openSnack: false,
      openErrorSnack: false,
      errorMessage: ''
    };
  }

  handleClick = () => {
    const {
      slug, size, quantity, price, cart, id
    } = this.props;
    // validate
    if (size) {
      // check quantityLimit
      const itemNum = _.where(cart, { slug });
      const itemCount = _.reduce(itemNum, (memo, item) => (memo + item.quantity), 0);

      if ((itemCount + quantity) > quantityLimit) {
        this.setState({
          openErrorSnack: true,
          openSnack: false,
          errorMessage: 'Cannot add more than 5 of the same item'
        });
      } else {
        const cartItem = {
          slug,
          id,
          size,
          quantity,
          price
        };
        this.props.addToCart(cartItem); // eslint-disable-line
        // this.setState({
        //   openSnack: true,
        //   openErrorSnack: false
        // });
      }
    } else {
      this.setState({
        openErrorSnack: true,
        openSnack: false,
        errorMessage: size ? 'Please select a color' : 'Please select a size'
      });
    }
  }

  closeSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ openSnack: false });
  }

  closeErrorSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ openErrorSnack: false });
  }

  render() {
    const { classes } = this.props;
    const { openSnack, openErrorSnack, errorMessage } = this.state;
    return (
      <div className={styles.wrapper}>
        <button
          type="button"
          className="btn __dark __full"
          onClick={this.handleClick}
        >
          ADD TO CART
        </button>
        <Snackbar
          key="add-to-cart-snack"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={openSnack}
          autoHideDuration={4500}
          onClose={this.closeSnack}
          className={classes.snackBar}
        >
          <SnackbarContent
            key="add-to-cart-snack-content"
            message={<span className={classes.message} id="message-id">Item added to cart</span>}
            action={[
              <NavLink to="/cart">
                view
              </NavLink>,
              <CloseIcon
                key="add-to-cart-snack-action"
                className={classes.closeIcon}
                onClick={this.closeSnack}
              />
            ]}
          />
        </Snackbar>
        <Snackbar
          key="add-to-cart-snack-error"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={openErrorSnack}
          autoHideDuration={4500}
          onClose={this.closeErrorSnack}
          className={classes.snackBar}
        >
          <SnackbarContent
            key="add-to-cart-snack-error-content"
            className={classes.snackError}
            message={<span className={classes.message} id="message-id">{errorMessage}</span>}
            action={[
              <CloseIcon
                key="add-to-cart-snack-action"
                className={classes.closeIcon}
                onClick={this.closeErrorSnack}
              />
            ]}
          />
        </Snackbar>
      </div>
    );
  }
}

ConnectedAddToCart.propTypes = {
  slug: PropTypes.string.isRequired,
  size: PropTypes.object,
  quantity: PropTypes.number,
  id: PropTypes.string,
  classes: PropTypes.object.isRequired,
  price: PropTypes.number.isRequired,
  cart: PropTypes.array.isRequired
};

ConnectedAddToCart.defaultProps = {
  size: null,
  quantity: 1,
  id: null
};

const AddToCart = connect(mapStateToProps, mapDispatchToProps)(withStyles(snackStyles)(ConnectedAddToCart)); // eslint-disable-line
export default AddToCart;
