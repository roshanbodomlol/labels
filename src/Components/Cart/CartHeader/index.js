import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import CartIcon from '@material-ui/icons/ShoppingCart';
import Badge from '@material-ui/core/Badge';

import styles from './styles.module.scss';

const mapStateToProps = state => (
  {
    cart: state.cart
  }
);

const ConnectedCartHeader = ({ cart }) => (
  <NavLink to="/cart" className={styles.cartButtonLink}>
    <Badge badgeContent={cart ? cart.length : ''}>
      <CartIcon />
    </Badge>
  </NavLink>
);

ConnectedCartHeader.propTypes = {
  cart: PropTypes.arrayOf(PropTypes.object)
};

ConnectedCartHeader.defaultProps = {
  cart: null
};

const CartHeader = connect(mapStateToProps, null, null, { pure: false })(ConnectedCartHeader);
export default CartHeader;
