import React from 'react';
import PropTypes from 'prop-types';

import { BlockBody } from '../../Layout/Blocks';

const CartItems = ({ items }) => (
  <React.Fragment>
    {
      items.length > 0
        ? items
        : (
          <BlockBody>
            <span>You have no items in your cart</span>
          </BlockBody>
        )
    }
  </React.Fragment>
);

CartItems.propTypes = {
  items: PropTypes.arrayOf(PropTypes.node).isRequired
};

export default CartItems;
