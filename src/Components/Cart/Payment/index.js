import React, { Component } from 'react';
import {
  NavLink, Switch, Route, Redirect
} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';
import axios from 'axios';
import _ from 'underscore';

import store from '../../../Store';
import { showGlobalSnack, emptyCart } from '../../../Store/actions';
import { placeOrder } from '../../../globalSettings';
import { BlockTitle, BlockBody } from '../../Layout/Blocks';
import EBanking from './EBanking';
import COD from './COD';

import styles from '../styles.module.scss';

const mapStateToProps = state => ({
  user: state.user,
  shippingInfo: state.shippingInfo,
  cart: state.cart
});

class Payment extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      gatewayUrl: '',
      amt: '',
      tAmt: '',
      scd: '',
      pid: '',
      su: '',
      fu: '',
      txAmt: ''
    };
    this.formRef = React.createRef();
  }

  componentDidMount() {
    const { user, shippingInfo, history } = this.props;
    const billingOK = user && !_.isEmpty(user.first_name)
      && !_.isEmpty(user.first_name)
      && !_.isEmpty(user.email)
      && !_.isEmpty(user.mobile)
      && !_.isEmpty(user.dob)
      && !_.isEmpty(user.city)
      && !_.isEmpty(user.state)
      && !_.isEmpty(user.street)
      && !_.isEmpty(user.country)
      && !_.isEmpty(user.zip);
    let shippingOK = false;
    _.each(shippingInfo, (infoItem) => {
      shippingOK = !_.isEmpty(infoItem);
    });
    if (billingOK && shippingOK) {
      // continue
    } else {
      store.dispatch(showGlobalSnack('Not enough information'));
      history.push('/cart');
    }
  }

  placeOrder = (paymentMode) => {
    const {
      discount, cart, shippingInfo, history
    } = this.props;
    const order = {
      order_detail: {
        coupon_code: discount['type'] === 'coupon' && discount.code, // eslint-disable-line
        promo_code: discount['type'] === 'promo' && discount.code, // eslint-disable-line
      },
      product_details: cart.map(item => ({
        product_id: item.id,
        product_quantity: item.quantity
      })),
      delivery_details: {
        first_name: shippingInfo.firstName,
        last_name: shippingInfo.lastName,
        phone: shippingInfo.phone,
        mobile: shippingInfo.mobile,
        street: shippingInfo.street,
        city: shippingInfo.city,
        state: shippingInfo.state,
        country: shippingInfo.country,
        zip: shippingInfo.zip
      },
      payment_details: {
        payment_mode: paymentMode,
        payment_status: ''
      }
    };
    const postOrder = JSON.stringify(order);

    // post order
    const token = Cookie.get('lbl_token');
    if (token) {
      this.setState({
        loading: true
      });
      const profileUpdateBody = new FormData();
      profileUpdateBody.set('token', token.toString());
      profileUpdateBody.set('order', postOrder);
      axios({
        url: placeOrder,
        method: 'POST',
        data: profileUpdateBody
      })
        .then((response) => {
          console.log(response.data);
          if (response.data.type === 'success') {
            if (paymentMode === 'cod') {
              store.dispatch(showGlobalSnack('Your order has been received. We will contact you as soon as possible.'));
              store.dispatch(emptyCart());
              history.push('/');
            } else {
              store.dispatch(showGlobalSnack('Redirecting to payment gateway...'));
              this.setState({
                gatewayUrl: response.data.message.link,
                amt: response.data.message.amount,
                tAmt: response.data.message.totalamount,
                txAmt: response.data.message.tax_amount,
                scd: response.data.message.esewa_merchant_id,
                pid: response.data.message.order_id,
                su: response.data.message.success_url,
                fu: response.data.message.failure_url,
                psc: response.data.message.servicecharge,
                pdc: response.data.message.deliverycharge
              });
              // history.push(response.data.message);
              // console.log(this.formRef);
              this.formRef.current.submit();
              // store.dispatch(emptyCart());
            }
          } else throw new Error('Please try again');
        })
        .catch((err) => {
          this.setState({ loading: false });
          console.log(err);
        });
    }
  }

  render() {
    const {
      loading, gatewayUrl, amt, tAmt, scd, pid, su, fu, pdc, psc, txAmt
    } = this.state;
    return (
      <div className="row">
        <div className="col-sm-3">
          <div className={styles.paymentOptionsSidebar}>
            <ul>
              <li>
                <NavLink exact to="/cart/payment/e-banking">E-Banking</NavLink>
              </li>
              <li>
                <NavLink exact to="/cart/payment/cash-on-delivery">Cash On Delivery</NavLink>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-sm-9">
          <BlockTitle primary={<h4>PAYMENT METHOD</h4>} />
          <BlockBody>
            <div className={styles.paymentBlockInner}>
              <Switch>
                <Redirect exact from="/cart/payment" to="/cart/payment/e-banking" />
                <Route path="/cart/payment/e-banking" exact render={props => <EBanking {...props} loading={loading} placeOrder={this.placeOrder} />} />
                <Route path="/cart/payment/cash-on-delivery" exact render={props => <COD {...props} loading={loading} placeOrder={this.placeOrder} />} />
              </Switch>
            </div>
          </BlockBody>
        </div>
        <div className="process-form" style={{ display: 'none' }}>
          <form action={gatewayUrl} method="POST" ref={this.formRef}>
            <input type="hidden" value={amt} name="amt" />
            <input type="hidden" value={tAmt} name="tAmt" />
            <input type="hidden" value={scd} name="scd" />
            <input type="hidden" value={pid} name="pid" />
            <input type="hidden" value={su} name="su" />
            <input type="hidden" value={fu} name="fu" />
            <input type="hidden" value={pdc} name="pdc" />
            <input type="hidden" value={psc} name="psc" />
            <input type="hidden" value={txAmt} name="txAmt" />
          </form>
        </div>

      </div>
    );
  }
}

Payment.propTypes = {
  discount: PropTypes.object.isRequired,
  cart: PropTypes.array.isRequired,
  user: PropTypes.any
};

Payment.defaultProps = {
  user: null
};

export default connect(mapStateToProps)(Payment);
