import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../../Layout/Button';
import styles from '../styles.module.scss';

class COD extends Component {
  handlePayment = () => {
    const { placeOrder } = this.props;
    placeOrder('cod');
  };

  render() {
    const { loading } = this.props;
    return (
      <div className={styles.cashondelivery}>
        <Button size="medium" onClick={() => { this.handlePayment('cod'); }} loading={loading}>
          CASH ON DELIVERY
        </Button>
      </div>
    );
  }
}

COD.propTypes = {
  loading: PropTypes.bool.isRequired,
  placeOrder: PropTypes.func.isRequired
};

export default COD;
