import React, { Component } from 'react';
import CheckIcon from '@material-ui/icons/CheckCircleRounded';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import store from '../../../Store';
import { showGlobalSnack } from '../../../Store/actions';
import Button from '../../Layout/Button';
import esewa from '../../../Static/img/ebanking/esewa.png';

import styles from '../styles.module.scss';

const iconStyles = {
  check: {
    color: '#2ecc71'
  }
};

class EBanking extends Component {
  state = {
    gateways: [
      {
        img: esewa,
        slug: 'esewa'
      }
    ],
    selectedGateway: {
      img: esewa,
      slug: 'esewa'
    }
  };

  selectGateway = (gateway) => {
    this.setState({
      selectedGateway: gateway
    });
  }

  handlePayment = () => {
    const { selectedGateway } = this.state;
    const { placeOrder } = this.props;
    if (selectedGateway) {
      placeOrder(selectedGateway.slug);
    } else {
      store.dispatch(showGlobalSnack('Please select a payment method'));
    }
  }

  render() {
    const { gateways, selectedGateway } = this.state;
    const { classes, loading } = this.props;
    const gatewayList = gateways.map((gateway, i) => (
      <div
        key={`e-banking-gateway-${i}`}
        className={styles.gateway}
        onClick={() => { this.selectGateway(gateway); }}
        role="button"
        tabIndex="-1"
      >
        {
          selectedGateway && selectedGateway.slug === gateway.slug
            ? (
              <div className={styles.checked}>
                <CheckIcon className={classes.check} />
              </div>
            )
            : ''
        }
        <img src={gateway.img} alt="" />
      </div>
    ));
    return (
      <div className={styles.ebanking}>
        <div className={styles.gatewayList}>
          {gatewayList}
        </div>
        <Button
          size="medium"
          onClick={this.handlePayment}
          loading={loading}
        >
          PAY NOW
        </Button>
      </div>
    );
  }
}

EBanking.propTypes = {
  classes: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  placeOrder: PropTypes.func.isRequired
};

export default withStyles(iconStyles)(EBanking);
