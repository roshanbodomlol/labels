import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'underscore';

import { BlockBody } from '../../Layout/Blocks';

import styles from '../styles.module.scss';

import { deliveryCharge } from '../../../globalSettings';

const mapStateToProps = state => (
  {
    items: state.cart
  }
);

const OrderSummary = ({
  items, discount, continueLink
}) => {
  const subTotal = _.reduce(items, (memo, item) => (memo + (item.price * item.quantity)), 0);
  return (
    <div className={styles.orderSummaryBlock}>
      <BlockBody noMargin secondary={<h4>Order Summary</h4>}>
        <div className={cn(styles.orderSummaryLine, 'd-flex justify-content-between')}>
          <span>Subtotal:</span>
          <span>Rs. {subTotal}</span>
        </div>
        {
          discount > 0
            ? (
              <div className={cn(styles.orderSummaryLine, 'd-flex justify-content-between')}>
                <span>Discount:</span>
                <span>Rs. {discount}</span>
              </div>
            )
            : ''
        }
        <div className={cn(styles.orderSummaryLine, 'd-flex justify-content-between')}>
          <span>Delivery Charge</span>
          <span>Rs. {deliveryCharge}</span>
        </div>
        <hr />
        <div className={cn(styles.orderSummaryLine, 'd-flex justify-content-between')}>
          <span><b>Total</b></span>
          <span><b>Rs. { subTotal + deliveryCharge - discount }</b></span>
        </div>
        {continueLink}
      </BlockBody>
    </div>
  );
};

OrderSummary.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  discount: PropTypes.number,
  continueLink: PropTypes.node
};

OrderSummary.defaultProps = {
  items: [],
  discount: 0,
  continueLink: ''
};

export default connect(mapStateToProps)(OrderSummary);
