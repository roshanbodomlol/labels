import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

import store from '../../../Store';
import { BlockBody } from '../../Layout/Blocks';
import Wrapper, { PlaceholderDiv } from '../../LoadingPlaceholder';
import { removeCartItem, addToWishList } from '../../../Store/actions';
import ConnectedSelector from '../ConnectedSelector';
import { apiUrl } from '../../../globalSettings';

import styles from './styles.module.scss';

const snackStyles = {
  message: {
    fontFamily: 'tex'
  }
};

const mapDispatchToProps = dispatch => (
  {
    removeItem: cartID => dispatch(removeCartItem(cartID))
  }
);

class ConnectedCartItem extends Component {
  constructor() {
    super();
    this.state = {
      currentItem: {},
      load: false,
      error: false,
      openSnack: false
    };
  }

  componentDidMount() {
    const { item } = this.props;
    const selection = {
      color: item.color,
      quantity: item.quantity.value,
      size: item.size.slug
    };
    axios
      .get(`${apiUrl}/products/getSingleProduct/${item.slug}`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            currentItem: {
              name: response.data.response.product_name,
              price: parseInt(response.data.response.price, 10),
              img: response.data.response.feature_image.thumb.url
            },
            load: true,
            ...selection
          });
        }
      })
      .catch((e) => {
        console.error(e, e.response);
        this.setState({ load: true, error: true });
      });
  }

  closeSnack = () => {
    this.setState({
      openSnack: false
    });
  }

  removeItemDialog = () => {
    this.setState({
      openSnack: true
    });
  }

  removeItem = () => {
    const { item, removeItem } = this.props;
    removeItem(item.cartID);
    this.closeSnack();
  }

  moveToWishlist = () => {
    const { item, removeItem } = this.props;
    removeItem(item.cartID);
    const wishlistItem = {
      slug: item.slug
    };
    store.dispatch(addToWishList(wishlistItem));
  }

  render() {
    const {
      currentItem, load, error, openSnack
    } = this.state;
    const { item, classes } = this.props;
    // const thumbnail = !_.isEmpty(currentItem) && _.findWhere(currentItem.img, { width: 250 });
    const thumbnail = !_.isEmpty(currentItem) && currentItem.img;
    return (
      <React.Fragment>
        {
          load
            ? (
              <BlockBody>
                <Snackbar
                  key="add-to-cart-snack"
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                  }}
                  open={openSnack}
                  onClose={this.closeSnack}
                >
                  <SnackbarContent
                    key="add-to-cart-snack-content"
                    message={<span className={classes.message} id="message-id">Are you sure?</span>}
                    action={[
                      <button className="btn __link dialogButton" onClick={this.removeItem}>Yes</button>,
                      <button className="btn __link dialogButton" onClick={this.closeSnack}>No</button>
                    ]}
                  />
                </Snackbar>
                <div className={styles.item}>
                  {
                    !error
                      ? (
                        <React.Fragment>
                          <img
                            src={thumbnail}
                            alt={currentItem.name}
                            // width={thumbnail.width}
                            // height={thumbnail.height}
                            style={{
                              maxWidth: 200,
                              maxHeight: 100
                            }}
                          />
                          <div className={styles.itemBody}>
                            <div className={styles.bodyInner}>
                              <div className={styles.left}>
                                <div className={styles.name}>
                                  <h4>{currentItem.name}</h4>
                                </div>
                                <div>
                                  {/* <span>
                                   Gender: { currentItem.gender }
                                  </span> */}
                                </div>
                                <div>
                                  <span>Size: {item.size.name}</span>
                                </div>
                                <div className={styles.selectWrap}>
                                  <ConnectedSelector item={item} />
                                </div>
                                <ul className="inline">
                                  <li>
                                    <button onClick={this.removeItemDialog} className="btn __link">Remove</button>
                                  </li>
                                  <li>
                                    <button className="btn __link" onClick={this.moveToWishlist}>Move to Wishlist</button>
                                  </li>
                                </ul>
                              </div>
                              <div className={styles.right}>
                                <div className={styles.price}>
                                  <h4>
                                    Rs. {currentItem.price}
                                  </h4>
                                </div>
                              </div>
                            </div>
                          </div>
                        </React.Fragment>
                      )
                      : ''
                  }
                </div>
              </BlockBody>
            )
            : (
              <div style={{ marginBottom: 15 }}>
                <Wrapper>
                  <PlaceholderDiv height={90} />
                </Wrapper>
              </div>
            )
        }
      </React.Fragment>
    );
  }
}

ConnectedCartItem.propTypes = {
  item: PropTypes.object.isRequired,
  removeItem: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
};

const CartItem = connect(null, mapDispatchToProps)(withStyles(snackStyles)(ConnectedCartItem));
export default CartItem;
