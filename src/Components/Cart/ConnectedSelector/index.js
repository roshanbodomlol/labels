import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'underscore';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';

import { updateItemQuantity } from '../../../Store/actions';
import Selector from '../../Layout/Selector';
import { quantityLimit } from '../../../globalSettings';

const mapDispatchToProps = dispatch => (
  {
    updateQuantity: (quantity, cartID) => dispatch(updateItemQuantity(quantity, cartID))
  }
);

const mapStateToProps = state => (
  {
    cart: state.cart
  }
);

const snackStyles = {
  message: {
    fontFamily: 'tex'
  },
  snackBar: {
    // marginTop: '15px'
  },
  snackError: {
    background: red[500]
  },
  closeIcon: {
    cursor: 'pointer'
  }
};

class ConnectedSelector extends Component {
  constructor() {
    super();
    this.state = {
      openErrorSnack: false,
      errorMessage: ''
    };
  }

  updateProductQuantity = (quantity) => {
    const { updateQuantity, cart, item } = this.props;
    // check quantityLimit
    const itemNum = _.where(cart, { slug: item.slug });
    // const quantityDiff = item.quantity - quantity;
    const itemCount = _.reduce(itemNum, (memo, itemI) => (memo + itemI.quantity), 0);
    if ((quantity - item.quantity + itemCount) > quantityLimit) {
      this.setState({
        openErrorSnack: true,
        errorMessage: 'Cannot add more than 5 of the same item'
      });
    } else {
      updateQuantity(quantity, item.cartID);
    }
  };

  closeErrorSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ openErrorSnack: false });
  }

  render() {
    const { item, classes } = this.props;
    const { openErrorSnack, errorMessage } = this.state;
    const quantityOptions = [];
    for (let i = 1; i <= quantityLimit; i++) {
      quantityOptions.push({
        name: i,
        value: i
      });
    }

    return (
      <React.Fragment>
        <Snackbar
          key={item.cartID}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={openErrorSnack}
          autoHideDuration={4500}
          onClose={this.closeErrorSnack}
          className={classes.snackBar}
        >
          <SnackbarContent
            key="add-to-cart-snack-error-content"
            className={classes.snackError}
            message={<span className={classes.message} id="message-id">{errorMessage}</span>}
            action={[
              <CloseIcon
                key="add-to-cart-snack-action"
                className={classes.closeIcon}
                onClick={this.closeErrorSnack}
              />
            ]}
          />
        </Snackbar>
        <Selector
          options={quantityOptions}
          updateSelect={(option) => {
            this.updateProductQuantity(option.value);
          }}
          selected={{ value: item.quantity, name: item.quantity }}
        />
      </React.Fragment>
    );
  }
}

ConnectedSelector.propTypes = {
  item: PropTypes.object.isRequired,
  updateQuantity: PropTypes.func.isRequired,
  cart: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired
};

const ConnectedSelectorWithStyles = connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(withStyles(snackStyles)(ConnectedSelector)); // eslint-disable-line
export default ConnectedSelectorWithStyles;
