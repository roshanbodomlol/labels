import React, { Component } from 'react';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import _ from 'underscore';
import axios from 'axios';
import { validateCoupon, getCoupons } from '../../../globalSettings';

import { BlockBody } from '../../Layout/Blocks';
import ThreeDots from '../../ThreeDots';

import styles from '../styles.module.scss';

const popupStyles = {
  paper: {
    borderRadius: 0
  },
  button: {
    cursor: 'pointer'
  }
};

class PromoCode extends Component {
  state = {
    isDialogOpen: false,
    discountCode: '',
    errors: {},
    loading: false,
    coupons: [],
    activeCoupon: null
  }

  componentDidMount() {
    axios({
      url: getCoupons,
      method: 'GET'
    })
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            coupons: response.data.response
          });
        } else throw new Error('Error receiving coupons');
      })
      .catch((e) => {
        console.log(e);
      });
  }

  closeDialog = () => {
    this.setState({
      isDialogOpen: false,
      errors: {},
      discountCode: ''
    });
  }

  openDialog = () => {
    this.setState({
      isDialogOpen: true
    });
  }

  checkError = (field) => {
    const { errors } = this.state;
    const fieldError = _.property(field)(errors);
    return fieldError && (
      <div className="with-error">
        { fieldError }
      </div>
    );
  }

  handleActiveCoupon = (coupon) => {
    this.setState((prevState) => {
      if (prevState.activeCoupon && prevState.activeCoupon.coupon_code === coupon.coupon_code) {
        return {
          activeCoupon: null
        };
      }
      return {
        activeCoupon: coupon
      };
    });
  }

  handleFormSubmit = (e) => {
    e.preventDefault();
    this.setState({
      errors: {},
      loading: true
    });
    const { setDiscount } = this.props;
    const { discountCode, activeCoupon } = this.state;

    if (activeCoupon) {
      setDiscount({
        type: 'coupon',
        code: activeCoupon.coupon_code,
        value: activeCoupon.coupon_value
      });
      this.setState({
        loading: false
      });
      this.closeDialog();
    } else {
      // get discount from code

      const couponBody = new FormData();
      couponBody.set('coupon_code', discountCode);
      axios({
        url: validateCoupon,
        method: 'POST',
        data: couponBody
      })
        .then((response) => {
          console.log(response.data);
          // setDiscount({
          //   type: 'promo',
          //   id: 'someID',
          //   value: discountValue
          // });
          this.setState({
            loading: false
          });
          this.closeDialog();
        })
        .catch((err) => {
          console.log(err);
          this.setState({
            loading: false,
            errors: {
              discountCode: 'The code you entered is invalid.'
            }
          });
        });
    }
  }

  render() {
    const { classes } = this.props;
    const {
      isDialogOpen, discountCode, loading, coupons, activeCoupon
    } = this.state;
    const couponList = coupons.map((coupon, i) => {
      const couponClasses = activeCoupon && coupon.coupon_code === activeCoupon.coupon_code ? classnames(styles.applyCoupon, 'active') : styles.applyCoupon;
      return (
        <div
          role="button"
          tabIndex="-1"
          className={couponClasses}
          key={`coupon-code-${i}`}
          onClick={() => { this.handleActiveCoupon(coupon); }}
        >
          <input type="text" value={coupon.coupon_code} disabled />
          Save
          <span><b> Rs {coupon.coupon_value}</b></span>
          <p>{coupon.coupon_name}</p>
        </div>
      );
    });
    return (
      <BlockBody secondary={<h4>Discount Code</h4>}>
        <Dialog
          open={isDialogOpen}
          onClose={this.closeDialog}
          classes={{ paper: classes.paper }}
        >
          <div className={styles.popupClose}>
            <CloseIcon onClick={this.closeDialog} className={classes.button} />
          </div>
          <div className={styles.promoPopupWrap}>
            <h4>ENTER COUPON CODE</h4>
            <form className="labels-form" onSubmit={(e) => { this.handleFormSubmit(e); }}>
              <div>
                <div className="control-row">
                  <div className="input-wrap">
                    {
                      this.checkError('discountCode')
                    }
                    <input
                      autoFocus // eslint-disable-line
                      className={styles.promoCodeInput}
                      type="text"
                      placeholder="Type discount or promo code"
                      value={discountCode}
                      onChange={(e) => { this.setState({ discountCode: e.target.value }); }}
                    />
                  </div>
                </div>
                <div className={styles.or}>or</div>
                <h4>APPLY COUPON</h4>
                {couponList}
              </div>
              <div className={styles.buttonGroup}>
                {
                  loading
                    ? (
                      <div>
                        <button type="button" disabled style={{ height: 48 }} className="btn __red __medium">
                          <ThreeDots />
                        </button>
                      </div>
                    )
                    : (
                      <div>
                        <button className="btn __red __medium">APPLY</button>
                      </div>
                    )
                }
                <div><button type="button" className="btn __red __medium" onClick={this.closeDialog}>CANCEL</button></div>
              </div>
            </form>
          </div>
        </Dialog>
        <div className={styles.promoButtonWrap}>
          <button className={classnames('btn __red __medium', classes.button)} onClick={this.openDialog}>APPLY</button>
        </div>
      </BlockBody>
    );
  }
}

PromoCode.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  setDiscount: PropTypes.func.isRequired
};

export default withStyles(popupStyles)(PromoCode);
