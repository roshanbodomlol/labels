import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import { BlockTitle } from '../../Layout/Blocks';
import CartItems from '../CartItems';
import CartItem from '../CartItem';
import PromoCode from '../PromoCode';
import OrderSummary from '../OrderSummary';

import styles from '../styles.module.scss';

const CartPageMain = ({
  items, setDiscount, discount, failDiscount
}) => {
  const cartItems = items.map((cartItem, index) => (
    <CartItem key={`cart-item-${index}`} item={cartItem} />
  ));
  return (
    <div className="row">
      <div className="col-md-9">
        <div className={styles.cartMain}>
          <BlockTitle
            primary={<h4>YOUR CART</h4>}
            action={<NavLink className={styles.shopLink} to="/">Continue Shopping</NavLink>}
          />
          <div className={styles.cartItemsWrap}>
            <CartItems items={cartItems} />
          </div>
        </div>
      </div>
      <div className="col-md-3">
        {
          cartItems.length > 0
            ? (
              <>
                <PromoCode setDiscount={setDiscount} fail={failDiscount} />
                <OrderSummary
                  discount={discount}
                  continueLink={(
                    <div className={styles.checkoutButtonWrap}>
                      <NavLink to="/cart/checkout" className="btn __dark __medium">CHECKOUT</NavLink>
                    </div>
                  )}
                />
              </>
            )
            : ''
        }
      </div>
    </div>
  );
};

CartPageMain.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  setDiscount: PropTypes.func.isRequired,
  failDiscount: PropTypes.func.isRequired,
  discount: PropTypes.number
};

CartPageMain.defaultProps = {
  items: [],
  discount: 0
};

export default CartPageMain;
