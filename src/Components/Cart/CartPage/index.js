import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import red from '@material-ui/core/colors/red';

import Checkout from '../Checkout';
import Payment from '../Payment';
import CartPageMain from './CartPageMain';

import styles from '../styles.module.scss';

const mapStateToProps = state => (
  {
    items: state.cart
  }
);

class ConnectedCartPage extends Component {
  state = {
    discount: {
      type: '',
      value: 0,
      code: undefined
    },
    snackOpen: false,
    snackIsError: false,
    snackMessage: ''
  };

  setDiscount = (discount) => {
    this.setState({
      discount,
      snackOpen: true,
      snackMessage: `Discount coupon for Rs. ${discount.value} has been applied`
    });
  }

  closeSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ snackOpen: false });
  }

  discountCodeFail = () => {
    this.setState({
      snackIsError: true,
      snackOpen: true,
      snackMessage: 'The discount coupon you used is invalid'
    });
  }

  render() {
    const { items } = this.props;
    const {
      discount, snackOpen, snackMessage, snackIsError
    } = this.state;
    return (
      <div id="main">
        <Snackbar
          key="login-component-error-snack"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={snackOpen}
          autoHideDuration={4500}
          onClose={this.closeSnack}
        >
          <SnackbarContent
            style={snackIsError ? { background: red } : {}}
            message={<span style={{ fontFamily: 'tex' }}>{snackMessage}</span>}
            action={[
              <CloseIcon
                style={{
                  cursor: 'pointer'
                }}
                onClick={this.close}
              />
            ]}
          />
        </Snackbar>
        <div className={styles.wrap}>
          <div className="container container-md">
            <div className="row">
              <div className="col">
                <div className={styles.cartHeader}>
                  <div className={styles.cartHeaderInner}>
                    <NavLink exact to="/cart">CART</NavLink>
                    <NavLink exact to="/cart/checkout">BILLING INFORMATION</NavLink>
                    <NavLink to="/cart/payment">PAYMENT</NavLink>
                  </div>
                </div>
              </div>
            </div>
            <Switch>
              <Route exact path="/cart" render={props => <CartPageMain {...props} items={items} failDiscount={this.discountCodeFail} discount={discount.value} setDiscount={this.setDiscount} />} />
              <Route exact path="/cart/checkout" render={props => <Checkout {...props} discount={discount.value} />} />
              <Route path="/cart/payment" render={props => <Payment {...props} discount={discount} />} />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

ConnectedCartPage.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
};

ConnectedCartPage.defaultProps = {
  items: []
};

const CartPage = connect(mapStateToProps, null, null, { pure: false })(ConnectedCartPage);
export default CartPage;
