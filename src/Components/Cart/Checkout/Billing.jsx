/* eslint-disable */
import React, { Component } from 'react';
import moment from 'moment';
import Cookie from 'js-cookie';
import axios from 'axios';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import OKIcon from '@material-ui/icons/CheckCircleOutline';
import { withStyles } from '@material-ui/core/styles';
import CalendarIcon from '@material-ui/icons/CalendarTodayOutlined';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';

import store from '../../../Store';
import { userLogin } from '../../../Store/actions';
import Form, { Input } from '../../Forms';
import { BlockBody, BlockTitle } from '../../Layout/Blocks';
import Button from '../../Layout/Button';
import LoginLocal from '../../LoginLocal';
import { updateUserProfile, createUser } from '../../../globalSettings';

import styles from '../styles.module.scss';

const iconStyles = {
  okIcon: {
    color: '#2ecc71'
  }
};

class Billing extends Component {
  constructor() {
    super();
    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      mobile: null,
      city: null,
      state: null,
      street: null,
      country: null,
      zip: null,
      password: null,
      passconf: null,
      loading: false,
      selectedDate: undefined,
      errors: {}
    };
    this.pickerDialogRef = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (this.props.isLoggedIn !== prevProps.isLoggedIn) {
      this.setState({
        firstName: this.props.user && this.props.user.first_name,
        lastName: this.props.user && this.props.user.last_name,
        email: this.props.user && this.props.user.email,
        mobile: this.props.user && this.props.user.mobile,
        company: this.props.user && this.props.user.company,
        city: this.props.user && this.props.user.city,
        state: this.props.user && this.props.user.state,
        street: this.props.user && this.props.user.street,
        country: this.props.user && this.props.user.country,
        zip: this.props.user && this.props.user.zip
      });
    }
  }

  componentDidMount() {
    this.setState({
      firstName: this.props.user && this.props.user.first_name,
      lastName: this.props.user && this.props.user.last_name,
      email: this.props.user && this.props.user.email,
      mobile: this.props.user && this.props.user.mobile,
      company: this.props.user && this.props.user.company,
      city: this.props.user && this.props.user.city,
      state: this.props.user && this.props.user.state,
      street: this.props.user && this.props.user.street,
      country: this.props.user && this.props.user.country,
      zip: this.props.user && this.props.user.zip
    });
  }

  handleInputChange = (label, e) => {
    this.setState({ [label]: e.target.value });
  };

  handlesubmit = (e) => {
    const {
      firstName,
      lastName,
      email,
      city,
      company,
      state,
      street,
      country,
      zip,
      mobile,
      password,
      passconf,
      selectedDate
    } = this.state;
    e.preventDefault();
    const token = Cookie.get('lbl_token');
    if (token) {
      this.setState({
        loading: true
      });
      const profileUpdateBody = new FormData();
      profileUpdateBody.set('token', token.toString());
      profileUpdateBody.set('first_name', firstName);
      profileUpdateBody.set('last_name', lastName);
      profileUpdateBody.set('email', email);
      profileUpdateBody.set('company', company);
      profileUpdateBody.set('city', city);
      profileUpdateBody.set('state', state);
      profileUpdateBody.set('street', street);
      profileUpdateBody.set('country', country);
      profileUpdateBody.set('zip', zip);
      axios({
        url: updateUserProfile,
        method: 'POST',
        data: profileUpdateBody
      })
        .then((response) => {
          if (response.data.type === 'success') {
            this.setState({
              // profile: response.data.response
              loading: false
            });
            store.dispatch(userLogin(response.data.response));
          } else throw new Error(response.data.message);
        })
        .catch((err) => {
          this.setState({ loading: false });
          console.log(err);
        });
    } else {
      // register
      this.setState({ loading: true });
      const registerBody = new FormData();
      registerBody.set('first_name', firstName);
      registerBody.set('last_name', lastName);
      registerBody.set('email', email);
      registerBody.set('dob', moment(selectedDate).format('YYYY-MM-DD'));
      registerBody.set('mobile', mobile);
      registerBody.set('company', company);
      registerBody.set('city', city);
      registerBody.set('state', state);
      registerBody.set('street', street);
      registerBody.set('country', country);
      registerBody.set('zip', zip);
      registerBody.set('password', password);
      registerBody.set('passconf', passconf);
      axios({
        url: createUser,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        data: registerBody
      })
        .then((response) => {
          if (response.data.type === 'success') {
            this.setState({ loading: false });
            Cookie.set('lbl_token', response.data.response.identifier, { expires: 1, path: '/' });
            store.dispatch(userLogin(response.data.response));
            this.closeDialog();
            this.clearForm();
          } else {
            this.setState({
              loading: false,
              errors: response.data.message
            });
          }
        })
        .catch((err) => {
          console.log(err);
          this.setState({ loading: false });
        });
    }
  }

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  handleDateChange = (date) => {
    this.setState({ selectedDate: date });
  };

  render() {
    const { isLoggedIn, ok, classes } = this.props;
    const {
      firstName,
      lastName,
      email,
      mobile,
      city,
      company,
      state,
      street,
      country,
      zip,
      password,
      passconf,
      loading,
      errors,
      selectedDate
    } = this.state;
    return (
      <div className="billing-information-section">
        <BlockTitle
          primary={
            <>
              <h4>BILING INFORMATION</h4>
              {
                ok
                  ? <OKIcon className={classes.okIcon} />
                  : <ErrorIcon />
              }
            </>
          }
        />
        <BlockBody>
          <div className={styles.checkoutForm}>
            <Form onSubmit={(e) => { this.handlesubmit(e); }}>
              <div className="control-row row">
                <div className="col-md">
                  <Input
                    type="text"
                    name="first_name"
                    placeholder="First Name"
                    value={firstName || ''}
                    errors={errors}
                    required
                    onChange={(e) => { this.setState({ firstName: e.target.value }); }}
                  />
                </div>
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="Last Name"
                    name="last_name"
                    value={lastName || ''}
                    errors={errors}
                    required
                    onChange={(e) => { this.setState({ lastName: e.target.value }); }}
                  />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input
                    type="email"
                    placeholder="Email"
                    readOnly={isLoggedIn}
                    required
                    name="email"
                    value={email || ''}
                    errors={errors}
                    onChange={(e) => { this.setState({ email: e.target.value }); }}
                  />
                </div>
                <div className="col-md">
                  <Input
                    type="tel"
                    placeholder="Phone Number"
                    name="mobile"
                    readOnly={isLoggedIn}
                    required
                    value={mobile || ''}
                    errors={errors}
                    onChange={(e) => { this.setState({ mobile: e.target.value }); }}
                  />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="Company"
                    value={company || ''}
                    errors={errors}
                    name="company"
                    onChange={(e) => { this.setState({ company: e.target.value }); }}
                  />
                </div>
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="City"
                    value={city || ''}
                    errors={errors}
                    name="city"
                    onChange={(e) => { this.setState({ city: e.target.value }); }}
                  />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="State"
                    value={state || ''}
                    errors={errors}
                    name="state"
                    onChange={(e) => { this.setState({ state: e.target.value }); }}
                  />
                </div>
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="Street"
                    value={street || ''}
                    errors={errors}
                    name="street"
                    onChange={(e) => { this.setState({ street: e.target.value }); }}
                  />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="Country"
                    value={country || ''}
                    errors={errors}
                    name="country"
                    onChange={(e) => { this.setState({ country: e.target.value }); }}
                  />
                </div>
                <div className="col-md">
                  <Input
                    type="text"
                    placeholder="ZIP Code"
                    value={zip || ''}
                    errors={errors}
                    name="zip"
                    onChange={(e) => { this.setState({ zip: e.target.value }); }}
                  />
                </div>
              </div>
              {
                isLoggedIn
                  ? ''
                  : (
                    <>
                      <div className="control-row row">
                        <div className="col-md">
                          <Input
                            onClick={this.openPickerDialog}
                            name="dob"
                            type="text"
                            placeholder="Date Of Birth"
                            required
                            errors={errors}
                            value={selectedDate && moment(selectedDate).format('Do MMM YYYY')}
                            other={(
                              <div className={styles.calendarIcon}>
                                <CalendarIcon
                                  style={{
                                    position: 'absolute',
                                    right: 6,
                                    top: 13,
                                    color: '#DADADA',
                                    pointerEvents: 'none'
                                  }}
                                />
                              </div>
                            )}
                          />
                          <div style={{ display: 'none' }}>
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                              <DatePicker
                                ref={this.pickerDialogRef}
                                margin="normal"
                                label="Date Of Birth"
                                value={selectedDate}
                                onChange={this.handleDateChange}
                                style={{
                                  width: '100%'
                                }}
                              />
                            </MuiPickersUtilsProvider>
                          </div>
                        </div>
                        <div className="col-md" />
                      </div>
                      <div className="control-row row">
                        <div className="col-md">
                          <Input
                            type="password"
                            placeholder="Password"
                            value={password || ''}
                            errors={errors}
                            name="password"
                            onChange={(e) => { this.setState({ password: e.target.value }); }}
                          />
                        </div>
                        <div className="col-md">
                          <Input
                            type="password"
                            placeholder="Confirm Password"
                            value={passconf || ''}
                            errors={errors}
                            name="passconf"
                            onChange={(e) => { this.setState({ passconf: e.target.value }); }}
                          />
                        </div>
                      </div>
                    </>
                  )
              }
              <div className="d-flex justify-content-end">
                {
                  isLoggedIn
                    ? (
                      <Button
                        size="medium"
                        type="submit"
                        mode="primary"
                        loading={loading}
                      >
                        SAVE
                      </Button>
                    )
                    : (
                      <span className={styles.buttons}>
                        <div>
                          <Button
                            size="medium"
                            type="submit"
                            mode="primary"
                            loading={loading}
                            onClick={this.handleRegister}
                          >
                            CREATE ACCOUNT
                          </Button>
                        </div>
                        <div>
                          <LoginLocal classes={classnames('btn __red')} />
                        </div>
                      </span>
                    )
                }
              </div>
            </Form>
          </div>
        </BlockBody>
      </div>
    );
  }
}

Billing.propTypes = {
  user: PropTypes.object,
  isLoggedIn: PropTypes.bool.isRequired,
  ok: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired
};

Billing.defaultProps = {
  user: null
};

export default withStyles(iconStyles)(Billing);
