import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import OKIcon from '@material-ui/icons/CheckCircleOutline';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

import store from '../../../Store';
import { updateShipping } from '../../../Store/actions';
import Form, { Input } from '../../Forms';
import { BlockBody, BlockTitle } from '../../Layout/Blocks';
import Button from '../../Layout/Button';

import styles from '../styles.module.scss';

const iconStyles = {
  okIcon: {
    color: '#2ecc71'
  }
};

const mapStateToProps = state => ({
  shippingInfo: state.shippingInfo
});

class Delivery extends Component {
  state = {
    firstName: '',
    lastName: '',
    phone: '',
    mobile: '',
    street: '',
    city: '',
    state: '',
    country: '',
    zip: ''
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const {
      street, city, state, country, zip, firstName, lastName, phone, mobile
    } = this.state;
    store.dispatch(updateShipping({
      firstName,
      lastName,
      phone,
      mobile,
      street,
      city,
      state,
      country,
      zip
    }));
  }

  render() {
    const { classes, shippingInfo } = this.props;
    const {
      street, city, state, country, zip, firstName, lastName, phone, mobile
    } = this.state;
    let ok = false;
    if (shippingInfo
      && shippingInfo.street
      && shippingInfo.city
      && shippingInfo.state
      && shippingInfo.country
      && shippingInfo.zip) {
      ok = true;
    }
    return (
      <div className="delivery-information-section">
        <BlockTitle
          primary={
            <>
              <h4>DELIVERY INFORMATION</h4>
              {
                ok
                  ? <OKIcon className={classes.okIcon} />
                  : <ErrorIcon />
              }
            </>
          }
        />
        <BlockBody noMargin>
          <div className={styles.checkoutForm}>
            <Form onSubmit={this.handleSubmit}>
              <div className="control-row row">
                <div className="col-md">
                  <Input name="firstName" required type="text" placeholder="First Name" value={firstName} onChange={(e) => { this.setState({ firstName: e.target.value }); }} />
                </div>
                <div className="col-md">
                  <Input name="lastName" required type="text" placeholder="Last Name" value={lastName} onChange={(e) => { this.setState({ lastName: e.target.value }); }} />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input name="phone" type="text" placeholder="Phone" value={phone} onChange={(e) => { this.setState({ phone: e.target.value }); }} />
                </div>
                <div className="col-md">
                  <Input name="mobile" required type="text" placeholder="Mobile" value={mobile} onChange={(e) => { this.setState({ mobile: e.target.value }); }} />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input name="street" required type="text" placeholder="Street" value={street} onChange={(e) => { this.setState({ street: e.target.value }); }} />
                </div>
                <div className="col-md">
                  <Input name="city" required type="text" placeholder="City" value={city} onChange={(e) => { this.setState({ city: e.target.value }); }} />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input name="state" required type="text" placeholder="State" value={state} onChange={(e) => { this.setState({ state: e.target.value }); }} />
                </div>
                <div className="col-md">
                  <Input name="country" required type="text" placeholder="Country" value={country} onChange={(e) => { this.setState({ country: e.target.value }); }} />
                </div>
              </div>
              <div className="control-row row">
                <div className="col-md">
                  <Input name="zip" required type="text" placeholder="Zip Code" value={zip} onChange={(e) => { this.setState({ zip: e.target.value }); }} />
                </div>
                <div className="col-md" />
              </div>
              <div className="d-flex justify-content-end">
                <Button
                  type="submit"
                  mode="primary"
                  size="medium"
                >
                  SAVE
                </Button>
              </div>
            </Form>
          </div>
        </BlockBody>
      </div>
    );
  }
}

Delivery.propTypes = {
  classes: PropTypes.object.isRequired,
  shippingInfo: PropTypes.object
};

Delivery.defaultProps = {
  shippingInfo: null
};

export default connect(mapStateToProps)(withStyles(iconStyles)(Delivery));
