import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'underscore';

import store from '../../../Store';
import { showGlobalSnack } from '../../../Store/actions';
import OrderSummary from '../OrderSummary';
import Billing from './Billing';
import Delivery from './Delivery';

import styles from '../styles.module.scss';

const mapStateToProps = state => (
  {
    isLoggedIn: state.isLoggedIn,
    items: state.cart,
    user: state.user,
    shippingInfo: state.shippingInfo
  }
);

class Checkout extends Component {
  constructor() {
    super();
    this.state = {
      billingOK: false,
      shippingOK: false
    };
  }

  componentDidMount() {
    const { user } = this.props;
    if (user) this.checkBillingProfile();
  }

  componentDidUpdate(prevProps) {
    // const { isLoggedIn } = this.props;
    if (!_.isEqual(prevProps, this.props)) {
      this.checkBillingProfile();
    }
  }

  checkBillingProfile = () => {
    const { user, shippingInfo } = this.props;
    const billingOK = !_.isEmpty(user.first_name)
      && !_.isEmpty(user.first_name)
      && !_.isEmpty(user.email)
      && !_.isEmpty(user.mobile)
      && !_.isEmpty(user.dob)
      && !_.isEmpty(user.city)
      && !_.isEmpty(user.state)
      && !_.isEmpty(user.street)
      && !_.isEmpty(user.country)
      && !_.isEmpty(user.zip);
    let shippingOK = false;
    _.each(shippingInfo, (infoItem) => {
      shippingOK = !_.isEmpty(infoItem);
    });
    this.setState({
      billingOK,
      shippingOK
    });
  }

  continueToPayment = () => {
    const { billingOK, shippingOK } = this.state;
    const { history } = this.props;
    if (billingOK && shippingOK) {
      history.push('/cart/payment');
    } else {
      // store.dispatch
      store.dispatch(showGlobalSnack('Please fill out your information first'));
    }
  }

  render() {
    const { isLoggedIn, discount, user } = this.props;
    const { billingOK } = this.state;
    return (
      <div className="row">
        <div className="col-md-9">
          <div className={styles.cartMain}>
            <Billing
              isLoggedIn={isLoggedIn}
              user={user}
              ok={billingOK}
              // check={this.checkBillingProfile}
            />
            {
              isLoggedIn
                ? <Delivery profile={user} ok={billingOK} />
                : ''
            }
          </div>
        </div>
        <div className="col-md-3">
          <OrderSummary
            discount={discount}
            continueLink={(
              <div className={styles.checkoutButtonWrap}>
                <button className="btn __medium" type="button" onClick={this.continueToPayment}>CONTINUE</button>
              </div>
            )}
          />
        </div>
      </div>
    );
  }
}

Checkout.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  shippingInfo: PropTypes.object,
  discount: PropTypes.number,
  user: PropTypes.object
};

Checkout.defaultProps = {
  discount: 0,
  user: null,
  shippingInfo: null
};

export default connect(mapStateToProps)(Checkout);
