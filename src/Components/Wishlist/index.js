import React, { Component } from 'react';
import Cookie from 'js-cookie';
import { connect } from 'react-redux';

import WishlistItem from './WishlistItem';
import { BlockTitle, BlockBody } from '../Layout/Blocks';
import styles from './styles.module.scss';

const mapStateToProps = state => ({
  items: state.wishlist
});

class Wishlist extends Component {
  state = {}

  render() {
    const wishlistCookie = Cookie.get('labelsWishlist');
    const wishlistArray = wishlistCookie && JSON.parse(wishlistCookie);
    const wishList = wishlistArray && wishlistArray.map((item, index) => (
      <WishlistItem key={index} item={item} />
    ));
    return (
      <div id="main">
        <div className={styles.wrap}>
          <div className="container container-md">
            <div className="row">
              <div className="col-sm-6 offset-sm-3">
                <BlockTitle primary={<h4>WISHLIST</h4>} />
                {
                  wishList.length > 0
                    ? wishList
                    : (
                      <BlockBody>
                        Your wishlist is empty
                      </BlockBody>
                    )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Wishlist);
