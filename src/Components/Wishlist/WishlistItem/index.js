import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import axios from 'axios';
import store from '../../../Store';

import { BlockBody } from '../../Layout/Blocks';
import Wrapper, { PlaceholderDiv } from '../../LoadingPlaceholder';
import { removeWishlistItem } from '../../../Store/actions';
import { apiUrl } from '../../../globalSettings';

import styles from './styles.module.scss';

class WishlistItem extends Component {
  constructor() {
    super();
    this.state = {
      currentItem: {},
      load: false,
      error: false
    };
  }

  componentDidMount() {
    const { item } = this.props;
    axios
      .get(`${apiUrl}/products/getSingleProduct/${item.slug}`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            currentItem: {
              name: response.data.response.product_name,
              price: parseInt(response.data.response.price, 10),
              img: response.data.response.feature_image.thumb.url
            },
            load: true
          });
        }
      })
      .catch((e) => {
        console.error(e, e.response);
        this.setState({ load: true, error: true });
      });
  }

  removeItem = () => {
    const { item } = this.props;
    store.dispatch(removeWishlistItem(item.wishlistItemID));
  }

  render() {
    const {
      currentItem, load, error
    } = this.state;
    // const thumbnail = !_.isEmpty(currentItem) && _.findWhere(currentItem.img, { width: 250 });
    const thumbnail = !_.isEmpty(currentItem) && currentItem.img;
    return (
      <React.Fragment>
        {
          load
            ? (
              <BlockBody>
                <div className={styles.item}>
                  {
                    !error
                      ? (
                        <React.Fragment>
                          <img
                            src={thumbnail}
                            alt={currentItem.name}
                            // width={thumbnail.width}
                            // height={thumbnail.height}
                            style={{
                              maxWidth: 200,
                              maxHeight: 100
                            }}
                          />
                          <div className={styles.itemBody}>
                            <div className={styles.bodyInner}>
                              <div className={styles.left}>
                                <div className={styles.name}>
                                  <h4>{currentItem.name}</h4>
                                </div>
                                <div>
                                  {/* <span>
                                   Gender: { currentItem.gender }
                                  </span> */}
                                </div>
                                <ul className="inline">
                                  <li>
                                    <button onClick={this.removeItem} className="btn __link">Remove</button>
                                  </li>
                                  {/* <li>
                                    <button className="btn __link">Add to Cart</button>
                                  </li> */}
                                </ul>
                              </div>
                              <div className={styles.right}>
                                <div className={styles.price}>
                                  <h4>
                                    Rs. {currentItem.price}
                                  </h4>
                                </div>
                              </div>
                            </div>
                          </div>
                        </React.Fragment>
                      )
                      : ''
                  }
                </div>
              </BlockBody>
            )
            : (
              <div style={{ marginBottom: 15 }}>
                <Wrapper>
                  <PlaceholderDiv height={90} />
                </Wrapper>
              </div>
            )
        }
      </React.Fragment>
    );
  }
}

WishlistItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default WishlistItem;
