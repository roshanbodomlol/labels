import React, { Component } from 'react';
import axios from 'axios';

import ThumbnailList from '../Layout/ThumbnailListHorizontal';
import { apiUrl } from '../../globalSettings';

class ItemsYouMayLike extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
      loaded: false
    };
  }

  componentDidMount() {
    axios
      .get(`${apiUrl}/products/getSuggestProducts`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            products: response.data.response,
            loaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  render() {
    const { products, loaded } = this.state;
    return (
      <React.Fragment>
        {
          loaded
            ? (
              <ThumbnailList title="Items You May Like" listItems={products} />
            )
            : ''
        }
      </React.Fragment>
    );
  }
}

export default ItemsYouMayLike;
