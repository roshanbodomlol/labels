import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import InfoIcon from '@material-ui/icons/PriorityHighRounded';
import Cookie from 'js-cookie';

import img from '../../Static/img/popup.jpg';
import styles from './styles.module.css';

const popupStyles = {
  paper: {
    maxWidth: 'none',
    borderRadius: 0
  },
  button: {
    color: '#fff',
    cursor: 'pointer',
    position: 'absolute',
    right: 9,
    top: 9
  },
  infoButton: {
    cursor: 'pointer',
    color: '#fff',
    transform: 'rotateZ(180deg)'
  }
};

class Popup extends Component {
  state = {
    isActive: false
  };

  componentDidMount() {
    const popupCookie = Cookie.get('lbl_popup');
    if (!popupCookie) {
      Cookie.set('lbl_popup', 1, { expires: 7, path: '/' });
      this.setState({
        isActive: true
      });
    }
  }

  closeDialog = () => {
    this.setState({
      isActive: false
    });
  }

  openDialog = () => {
    this.setState({
      isActive: true
    });
  }

  render() {
    const { classes } = this.props;
    const { isActive } = this.state;
    return (
      <>
        <Dialog
          open={isActive}
          onClose={this.closeDialog}
          classes={{ paper: classes.paper }}
        >
          <CloseIcon onClick={this.closeDialog} className={classes.button} />
          <img className={styles.popupImage} src={img} alt="" />
        </Dialog>
        {
          !isActive
            ? (
              <div className={styles.popupInfo}>
                <InfoIcon
                  onClick={() => { this.setState({ isActive: true }); }}
                  className={classes.infoButton}
                />
              </div>
            )
            : ''
        }
      </>
    );
  }
}

Popup.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

export default withStyles(popupStyles)(Popup);
