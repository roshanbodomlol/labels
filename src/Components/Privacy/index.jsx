/* eslint-disable */
import React from 'react';
import { NavLink } from 'react-router-dom';

import OneColumnPage from '../OneColumnPage';

const Privacy = () => (
  <OneColumnPage title="PRIVACY & COOKIES">
    <p>
      Welcome to the <NavLink to="http://www.labels.com">www.labels.com</NavLink> website (hereinafter referred to as the “Site”) owned by Roots fashion
  Pvt. Ltd. We are aware of your rights to privacy and would always work to safeguard your rights. To
  learn more about our Privacy Policy, please go through this document.
    </p>
    <p>
    This Privacy Policy explains the collection, usage, and disclosure of your information, under certain
    conditions and your options. This Privacy Policy also explains about the efforts we have made to secure
    your personal information. You accept the terms described herein by visiting the Site directly or
    indirectly through other sites.
    </p>
    <p>
    We value your privacy and trust and shall therefore only use information provided by you in the ways
    stated here. We will only collect information necessary for our operations and keep them till it is
    relevant to us or as required by the prevailing law.
    You can remain anonymous and browse through our site without providing your personal details. We
    can’t identify you unless you have an account on the Site and you have logged on to it.
    </p>
    <br />
    <h4>1. Data we collect</h4>
    <p>
      If you proceed to place an order for a product with us on the Site, we may collect various data from you.
  We collect, process and store your personal details that include but is not limited to your name, email
  address, contact number, delivery address and payment details to process your order on the Site and to
  provide services to you.
    </p>
<p>
  We will use the information you provide to process your orders and give you our services and
  information offered through our Site upon your request. We will also use the information you provide to
  administer your account, verify and carry out transactions; improve layout and contents of pages of our
  website and customize them for users; identify visitors; carry out research on users’ demographics, send
  you information you requested from us or might find helpful, including details about our product and
  services. If you prefer not to receive any marketing communications, you can opt out at any time.
</p>
<p>
  We may share your name, address and contact number with a third party to deliver the product to you.
  You can view the details of your orders that have been placed, that are yet to be placed and that are
  open with your address, contact number and other details. You are expected not to make the details of
  the delivery and other details available to unauthorized third parties and maintain the confidentiality.
</p>
<h5>Other uses of your Personal Information</h5>
<p>
  We may use your opinions and your feedbacks for market research. This will only be used for statistical
purposes and will not be forwarded to third parties. We may send you other information about us, the

Site, our products, our newsletters, competitions, sales promotions and anything related to business. If
you prefer not to receive any of this additional information, please click ‘unsubscribe&#39; link in emails we
send to you.
</p>
<h5>competitions</h5>
<p>
  We might use your personal data to reach out when you win prizes or to advertise our offers. More
details about each offer will be mentioned with the terms to participate in the respective offers.
</p>
<h4>2. COOKIES</h4>
<p>
  Cookies are the messages exchanged between your web browser and web server when you visit
different sites. Cookies are used to identify the unique user through your Internet Protocol address that
makes it easier and saves time when you want to go through the Site. Cookies are stored by your
internet browser on your computer’s hard drive. We use cookies to make our service easier and
convenient for you and not to obtain or use any other information about you. You can set your browser
to not accept cookies, but this will make your browsing through the Site time consuming and tiring( for
instance, when you are about to place/ modify orders you might have to enter your details multiple
times). We would like to assure you that the cookies we collect don’t contain any hidden personal
details unless mentioned. You can find more about the usage and removal of your cookies through
http://www.allaboutcookies.org. The Site uses Google Analytics, a web analytics service provided by
Google Inc. (hereinafter referred to as &quot;Google&quot;). Google Analytics uses cookies to help the Site analyze
how users use the Site. The information generated while you are using the Site will be stored in Google
Servers in the United States. Google will use these cookies to evaluate your use of the Site, generate
reports for the Site operators. Google may also transfer this information to third parties if required by
the law. Google will not link your IP address with any information Google holds. By using the appropriate
setting you can refuse the use of cookies in your browser, but please note that you may not be able to
use the full functionality of the Site if you choose to turn off cookies. By using the Site, you agree to the
data processing by Google as mentioned above.
</p>
<h4>3. SECURITY</h4>
<p>
  We have adhered to different security and technical measures to prevent unlawful or unauthorized
access to or damaging, destruction or loss of your information. We collect your personal details on a
secure server and use necessary security procedures to safeguard them. We may request your proof of
identity at times before disclosing personal information to you. You are responsible to protect against
unauthorized access to your password or your computer.
</p>
  </OneColumnPage>
);

export default Privacy;
