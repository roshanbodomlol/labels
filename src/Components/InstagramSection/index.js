import React, { Component } from 'react';

import InstagramFeed from '../InstagramFeed';

import styles from './styles.module.scss';
import instaLogo from '../../Static/img/instagram_logo.png';

class InstagramSection extends Component {
  constructor() {
    super();
    this.state = {
      hashtag: 'nepal'
    };
  }

  render() {
    const { hashtag } = this.state;
    return (
      <div className={styles.feedSection}>
        <div className={styles.instaLogo}>
          <img src={instaLogo} alt="" />
          <span>INSTAGRAM FEED</span>
        </div>
        <p>
          Get trendy with LABELS
          <br />
          and share with us your style
        </p>
        <div className={styles.feedWrap}>
          <InstagramFeed hashtag={hashtag} thumbnailWidth={320} limit={5} />
        </div>
      </div>
    );
  }
}

export default InstagramSection;
