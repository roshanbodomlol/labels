/* eslint-disable */
import React from 'react';

import OneColumnPage from '../OneColumnPage';

const Accessibilty = () => (
  <OneColumnPage title="TERMS AND CONDITIONS">
    <h4>Overview</h4>
<p>Levels is committed to providing quality services to people from all backgrounds and abilities.
The company has the policy to provide a fair, respecting and accessible, enjoyable service and shopping experience to each and every
customer. No matter, if the customer is with disabilities or without disabilities, we serve everyone well.
Employee Training</p>
<p>
  All of our employees go through general professional training, to make shopping at levels accessible to all.
</p>

<h4>
  In-Store Accessibility
</h4>
<p>
  Levels operate at approximately 15 physical stores throughout the nation. Our store is accessible for all.
</p>

<h4>Accessibility Customer service center</h4>
<p>
  Customers wishing to contact our customer service center any day at any time for any information is welcome
  1. Fax:
  2. Phone:
  3. Email:
</p>

<h4>
  Contact Us
</h4>
<p>
  We will be glad to receive feedback and suggestion from you for improving our accessibility for all. You can communicate directly with
  our accessibility manager.
  Phone:
  Email:
</p>
  </OneColumnPage>
);

export default Accessibilty;
