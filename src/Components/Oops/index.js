import React from 'react';

import styles from './styles.module.scss';

const Oops = () => (
  <a href="/">
    <div className={styles.oops}>
        OOPS!
    </div>
  </a>
);

export default Oops;
