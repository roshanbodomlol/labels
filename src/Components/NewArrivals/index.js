import React, { Component } from 'react';
import axios from 'axios';

import ThumbnailList from '../Layout/ThumbnailListHorizontal';
import { apiUrl } from '../../globalSettings';

class NewArrivals extends Component {
  constructor() {
    super();
    this.state = {
      newArrivals: [],
      loaded: false
    };
  }

  componentDidMount() {
    axios
      .get(`${apiUrl}/products/getNewArrivals`)
      .then((response) => {
        if (response.data.type === 'success') {
          this.setState({
            newArrivals: response.data.response,
            loaded: true
          });
        }
      })
      .catch((e) => {
        console.log('Error getting categories', e);
      });
  }

  render() {
    const { newArrivals, loaded } = this.state;
    return (
      <React.Fragment>
        {
          loaded
            ? (
              <ThumbnailList title="New Arrivals" listItems={newArrivals} />
            )
            : ''
        }
      </React.Fragment>
    );
  }
}

export default NewArrivals;
