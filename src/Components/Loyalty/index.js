/* eslint-disable */
import React from 'react';

import OneColumnPage from '../OneColumnPage';

const Loyalty = () => (
  <OneColumnPage title="LABELS LOYALTY PROGRAM">
    <ul>
      <li>Points earned will be named Labels Coin.</li>
      <li>How to earn Label Coins
  <ol>
    <li>
      In purchases up to Rs.5000, 5% of the total amount is earned.
    </li>
    <li>
      In purchases up to Rs.10000, 10% of the total amount is earned.
    </li>
    <li>In purchases above Rs.10000, 12% of the total amount is earned.</li>
    <li>In purchases of SALE items, 5% of the total amount is earned.</li>
  </ol>
      </li>
      <li>The first time an account is registered 100 Label coins is awarded.</li>
      <li>Anytime a user recommends the app/site, he will be able to give a referral code to his friends or family. If this referral code is used he earns 100 Label coins. There should be a feature to switch this feature off anytime we like.</li>
      <li>Every time a user reviews an item, they earn 50 Label coins. There is no limit to how many items they can review but there is a limit of 150 Label coins per month that you can earn through reviews. Users who have reviewed the items they purchased will be called out as verified users.</li>
      <li>Label coins are redeemable only through purchases online and NOT in-store.</li>
      <li>Label coins will expire after a year if not used.</li>
      <li>Label coins are non-transferable.</li>
      <li>There will be a routine quiz (daily/weekly), where one selected user with the right answer will earn 50 Label coins.</li>
      <li>As part of the Loyalty program, the apps will have a 1 week prior announcements of in-store SALE.</li>
      <li>Active users will be rewarded 1000 Label coins on their Birthdays. This will not be advertised.</li>
    </ul>
  </OneColumnPage>
);

export default Loyalty;
