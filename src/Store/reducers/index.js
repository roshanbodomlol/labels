import Cookies from 'js-cookie';
import uuid from 'uuid';
import _ from 'underscore';
import { quantityLimit } from '../../globalSettings';

import {
  ADD_TO_CART,
  REMOVE_CART_ITEM,
  UPDATE_ITEM_QUANTITY,
  USER_LOGIN,
  USER_LOGOUT,
  OPEN_LOGIN_DIALOG,
  CLOSE_LOGIN_DIALOG,
  ADD_TO_WISHLIST,
  REMOVE_WISHLIST_ITEM,
  UPDATE_SHIPPING,
  SHOW_GLOBAL_SNACK,
  HIDE_GLOBAL_SNACK,
  EMPTY_CART
} from '../constants/action-types';

const cartCookie = Cookies.get('labelsCart');
const wishlistCookie = Cookies.get('labelsWishlist');

const initialState = {
  cart: cartCookie ? JSON.parse(cartCookie) : [],
  wishlist: wishlistCookie ? JSON.parse(wishlistCookie) : [],
  isLoggedIn: false,
  user: null,
  isLoginDialogOpen: false,
  globalSnack: false,
  globalSnackMessage: '',
  shippingInfo: null
};

const rootReducer = (state = initialState, action) => {
  let newState = {};
  switch (action.type) {
    case ADD_TO_CART: {
      const { cart } = state;
      const itemNum = _.where(cart, { slug: action.payload.slug });
      const itemCount = _.reduce(itemNum, (memo, item) => (memo + item.quantity), 0);
      if ((itemCount + action.payload.quantity) <= quantityLimit) {
        const cartID = uuid.v4();
        cart.push({
          ...action.payload,
          cartID
        });
        Cookies.set('labelsCart', JSON.stringify(cart), { expires: 7, path: '/' });
        newState = {
          ...state,
          cart,
          globalSnack: 'success',
          globalSnackMessage: 'Item added to cart'
        };
        return newState;
      }
      return {
        ...state,
        globalSnack: 'error',
        globalSnackMessage: 'Cannot add more than 5 of the same item'
      };
    }
    case REMOVE_CART_ITEM: {
      const { cartID } = action.payload;
      const { cart } = state;
      const newCart = _.reject(cart, { cartID });
      Cookies.set('labelsCart', JSON.stringify(newCart), { expires: 7, path: '/' });
      newState = {
        ...state,
        cart: newCart
      };
      return newState;
    }
    case UPDATE_ITEM_QUANTITY: {
      const { quantity, cartID } = action.payload;
      const { cart } = state;
      _.each(cart, (item, i) => {
        if (item.cartID === cartID) {
          cart[i].quantity = quantity;
        }
      });
      Cookies.set('labelsCart', JSON.stringify(cart), { expires: 7, path: '/' });
      newState = {
        ...state,
        cart
      };
      return newState;
    }
    case USER_LOGIN: {
      newState = {
        ...state,
        user: action.payload.user,
        isLoggedIn: true
      };
      console.log('reducer', action.payload.user);
      return newState;
    }
    case USER_LOGOUT: {
      newState = {
        ...state,
        user: null,
        isLoggedIn: false
      };
      return newState;
    }
    case OPEN_LOGIN_DIALOG: {
      newState = {
        ...state,
        isLoginDialogOpen: true
      };
      return newState;
    }
    case CLOSE_LOGIN_DIALOG: {
      newState = {
        ...state,
        isLoginDialogOpen: false
      };
      return newState;
    }
    case ADD_TO_WISHLIST: {
      const { wishlist } = state;
      if (_.find(wishlist, item => item.slug === action.payload.slug)) {
        return state;
      }
      const wishlistItemID = uuid.v4();
      wishlist.push({
        ...action.payload,
        wishlistItemID
      });
      Cookies.set('labelsWishlist', JSON.stringify(wishlist), { expires: 7, path: '/' });
      newState = {
        ...state,
        wishlist,
        globalSnack: true,
        globalSnackMessage: 'Item added to wishlist'
      };
      return newState;
    }
    case REMOVE_WISHLIST_ITEM: {
      const { wishlistItemID } = action.payload;
      const { wishlist } = state;
      const newList = _.reject(wishlist, { wishlistItemID });
      Cookies.set('labelsWishlist', JSON.stringify(newList), { expires: 7, path: '/' });
      newState = {
        ...state,
        wishlist: newList,
        globalSnack: true,
        globalSnackMessage: 'Item removed from wishlist'
      };
      return newState;
    }
    case UPDATE_SHIPPING: {
      const { shippingInfo } = action.payload;
      return {
        ...state,
        shippingInfo,
        globalSnack: true,
        globalSnackMessage: 'Information updated'
      };
    }
    case SHOW_GLOBAL_SNACK: {
      const { globalSnackMessage } = action.payload;
      return {
        ...state,
        globalSnack: true,
        globalSnackMessage
      };
    }
    case HIDE_GLOBAL_SNACK: {
      return {
        ...state,
        globalSnack: false
      };
    }
    case EMPTY_CART: {
      const newCart = [];
      Cookies.set('labelsCart', JSON.stringify(newCart), { expires: 7, path: '/' });
      return {
        ...state,
        cart: newCart
      };
    }
    default:
      return state;
  }
};

export default rootReducer;
