import {
  ADD_TO_CART,
  REMOVE_CART_ITEM,
  UPDATE_ITEM_QUANTITY,
  USER_LOGIN,
  USER_LOGOUT,
  OPEN_LOGIN_DIALOG,
  CLOSE_LOGIN_DIALOG,
  ADD_TO_WISHLIST,
  REMOVE_WISHLIST_ITEM,
  UPDATE_SHIPPING,
  SHOW_GLOBAL_SNACK,
  HIDE_GLOBAL_SNACK,
  EMPTY_CART
} from '../constants/action-types';

export const addToCart = item => ({
  type: ADD_TO_CART,
  payload: item
});

export const removeCartItem = cartID => ({
  type: REMOVE_CART_ITEM,
  payload: {
    cartID
  }
});

export const addToWishList = item => ({
  type: ADD_TO_WISHLIST,
  payload: item
});

export const removeWishlistItem = wishlistItemID => ({
  type: REMOVE_WISHLIST_ITEM,
  payload: {
    wishlistItemID
  }
});

export const updateItemQuantity = (quantity, cartID) => ({
  type: UPDATE_ITEM_QUANTITY,
  payload: {
    quantity,
    cartID
  }
});

export const userLogin = user => ({
  type: USER_LOGIN,
  payload: {
    user
  }
});

export const userLogout = () => ({
  type: USER_LOGOUT
});

export const openLoginDialog = () => ({
  type: OPEN_LOGIN_DIALOG
});

export const closeLoginDialog = () => ({
  type: CLOSE_LOGIN_DIALOG
});

export const updateShipping = info => ({
  type: UPDATE_SHIPPING,
  payload: {
    shippingInfo: info
  }
});

export const showGlobalSnack = message => ({
  type: SHOW_GLOBAL_SNACK,
  payload: {
    globalSnackMessage: message
  }
});

export const hideGlobalSnack = () => ({
  type: HIDE_GLOBAL_SNACK
});

export const emptyCart = () => ({
  type: EMPTY_CART
});
