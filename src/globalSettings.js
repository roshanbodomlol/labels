// API
const backend = 'http://labels.pagodalabs.com.np/api';
export const apiUrl = `${backend}`;
export const loginUrl = `${backend}/members/login`;
export const registerUrl = `${backend}/members/register`;
export const authenticateUrl = `${backend}/members/validateuser`;
export const getUserProfileData = `${backend}/members/profile`;
export const updateUserProfile = `${backend}/members/updateprofile`;
export const createUser = `${backend}/members/create`;
export const placeOrder = `${backend}/orders/place`;
export const getCoupons = `${backend}/coupons/getPromoCode`;
export const validateCoupon = `${backend}/coupons/validateCouponCode`;
export const getBrands = `${backend}/brands`;

// BREAKPOINTS
export const mobileBreakPoint = 1025;

// LIMITS
export const quantityLimit = 5;

export const deliveryCharge = 200;

// COOKIES
export const tokenExpireAfterDays = 14;
